<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
    <div class="sidebar col-md-3 col-sm-4 cats-bussines">
      <nav class="nav_bar">
        <div class="container-fluid cats-bussines">
          <div class="navmenu offcanvas-sm">
            <ul class="nav navmenu-nav nav-pills nav-stacked">
              <li><a href="/" class="back"><span class="icon-arrow-left2"></span> <span><?= __('Back to news', 'em-plugin')?></span></a></li>
              <li><a href="#" class="adclassy"><span class="glyphicon glyphicon-bullhorn"></span><span> <?= __('Place an ad', 'em-plugin')?></span></a></li>
              <?php foreach ($categories as $id=>$name ): ?>
              <li class="<?=(isset($_GET['catUrl']) and $_GET['catUrl']==$id)?'active':''?>"><a  href="/<?= $pageUrl?>?catUrl=<?=$id?> ">
                      <!--<img src="img/busforsale.png" class="icon">-->
                      <span><?=$name?></span></a>
                </li>

              <?php endforeach; ?>

            </ul>
              <form method="GET" class="navbar-form navbar-left" action="/<?=get_option('EM_searchUrl')?>" role="search">
              <div class="input-group">
                  <input type="text" class="form-control" value="<?=(isset($searchParam['searchStr']))?$searchParam['searchStr']:''?>" name="searchParam[searchStr]" placeholder="<?= __('Search', 'em-plugin')?>...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><span class="icon-search"></span></button>
                </span>
              </div>
            </form>
            <ul class="nav nav-pills nav-stacked dashed">
              <li><?= __('Advertising Service', 'em-plugin')?> <br><phone>877-702-0220</phone></li>
              <li><?= __('Feed ads', 'em-plugin')?> <br><phone>877-459-0909</phone></li>
            </ul>
          </div>
          <div class="navbar navbar-fixed-top hidden-md hidden-lg">
            <button type="button" class="navbar-toggle" data-toggle="offcanvas" data-target=".navmenu">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
        </div>
      </nav>
    </div>

