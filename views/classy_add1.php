            <input type="hidden" name='EM_Step' value='2' />
            <input type="hidden" name='action' value='EM_action' />
            <input type="hidden" name='AdvertiseUser[advertise_id]' value='<?=$adContent['id']?>' />
          <div class="form-group">
            <label for="category" class="col-sm-2 control-label"><?= __('Category', 'em-plugin')?>
                <span class="required">*</span>
                <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=__('Specify the category ad placement', 'em-plugin')?>">?</span></label>
            <div class="col-sm-10">
              <select class="form-control required_" id="AdvertiseUser_category_id" name="AdvertiseUser[category_id]">
                  <option value="0" class="disabled"><?=__('Select category', 'em-plugin')?></option>
                    <?php foreach ($adCategoryes as $id => $name ): ?>
                    <option <?=(isset($_SESSION['EM_advFormValue']['AdvertiseUser']['category_id']) and $id == $_SESSION['EM_advFormValue']['AdvertiseUser']['category_id'])?'selected=""':'' ?> value="<?= $id?>"><?= $name?></option>
                    <?php endforeach; ?>
              </select>
            </div>
          </div>

            <?php foreach ($adContent['fields'] as $field ): ?>
                <?php if ($field['name'] == 'Content' and $field['type'] == 'textarea'): ?>
                    <div class="form-group">
                        <label for="classytext" class="col-sm-2 control-label "><?=$field['name'] ?>
                            <span class="required">*</span> <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=$field['description'] ?>">?</span></label>
                      <div class="col-sm-10">
                        <textarea  class="form-control required_ EM_count" name="field[<?=$field['id']?>]" id="classytext"><?=(isset($_SESSION['EM_advFormValue']['field'][$field['id']]))?$_SESSION['EM_advFormValue']['field'][$field['id']]:""?></textarea>
                        <div class="classytext_counters">
                            <div class="counter"><?=__('Сharacters', 'em-plugin')?>: <span id="EM_chars"></span></div>
                          <div class="counter"><?=__('Words', 'em-plugin')?>: <span id="EM_words"></span></div>
                        </div>
                        <p><?=__('Items marked', 'em-plugin')?> <span class="required">*</span>, <?=__('required', 'em-plugin')?>.</p>
                      </div>
                    </div>
                <?php endif; ?>
                <?php if (in_array($field['type'], array('text', 'email', 'number', 'tel', 'checkbox', 'date', 'file'))): ?>
                    <div class="form-group">
                      <label for="field_<?=$field['id']?>" class="col-sm-2 control-label"><?=$field['name'] ?><?php if ($field['requre'] == 1): ?> <span class="required">*</span> <?php endif; ?> <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=$field['description'] ?>">?</span></label>
                      <div class="col-sm-10">
                            <?php $value = (isset($_SESSION['EM_advFormValue']['field'][$field['id']]))?$_SESSION['EM_advFormValue']['field'][$field['id']]:null;          ?>
                        <input class='EM_count form-control  <?php if ($field['requre'] == 1): ?>required_ <?php endif; ?>' id="field_<?=$field['id']?>"  type="<?= $field['type'] ?>"
                                   name="<?=($field['type']=='file')?'field_'.$field['id'].'':'field['.$field['id'].']'?>"
                               <?php if ($field['type'] == 'checkbox' and $value): ?>
                                   checked=""
                                   <?php endif; ?>
                               <?php if ($field['type'] != 'checkbox'): ?>
                                   value="<?= $value ?>"

                               <?php else: ?>
                                   value="1"
                               <?php endif; ?>
                               <?php if ($field['type'] == 'number'): ?>
                                   step="any"
                               <?php endif; ?>
                               <?php if ($field['requre'] == 1): ?>
                                   required="required"
                               <?php endif; ?>
								   title="<?=($field['description'])?$field['description']:$field['name'] ?>"
                               />
                        </div>
                    </div>
                <?php endif; ?>

            <?php endforeach; ?>
            <div id="EM_alert" class="alert alert-danger hidden" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <strong><?= __('Correct the form errors', 'em-plugin')?></strong>
                <ul id="EM_errors" class=""></ul>
            </div>
          <div class="form-group">
            <div class="col-sm-12">
                <a href="" id="EM_prev" class="btn btn-default disabled"><?= __('Prev', 'em-plugin')?></a>
                <a href="#" id="EM_next" onclick="" class="btn btn-primary pull-right"><?= __('Next', 'em-plugin')?></a>
            </div>
          </div>
            <script type="text/javascript">
                jQuery('#EM_progress-bar').css('width','10%');
                jQuery('.em_step').addClass('disabled');
                jQuery('.em_step1').removeClass('disabled');
                var EM_errors='';
                function validateForm(){

                    jQuery('.required_').removeClass('requiredBorder');
                    jQuery('.required_').each(function(){
                        if (! jQuery(this).val() || jQuery(this).val()==0 ) {
                            EM_errors=EM_errors+"<li><?= __('Blank value', 'em-plugin')?> </li>";
                            jQuery(this).addClass('requiredBorder');
                        }
                    });
                    jQuery('.EM_count').each(function(){
                        if (cFM_checkFullness(jQuery(this))) {
                            EM_errors=EM_errors+"<li><?= __('Wrong value', 'em-plugin')?> </li>";
                            jQuery(this).addClass('requiredBorder');
                        }
                    });
                    return EM_errors;
                }

                function cFM_checkFullness(obj){
                    var error ;
                    switch(obj.attr('type')){
                        case 'email':
                            var regCheck = new RegExp("^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
                            if(!regCheck.test(obj.val())){
                                error='wrong';
                            }
                            break;
                        case 'num':
                            var regCheck = new RegExp('[^0-9\s-]+');
                            if(regCheck.test(obj.val()))
                            error='wrong';
                            break;
                    }
                    return error;
                }


                function nextStep(){
                    errors=validateForm();
                    console.log(errors);
                    if (errors) {
                        jQuery('#EM_errors').html(errors);
                        jQuery('#EM_alert').removeClass('hidden');
                        EM_errors = '';
                    }else{
                        form = jQuery('#EM_FORM').get(0);
                        data=new FormData(form);
                        jQuery.ajax({
                          type: 'POST',
                          url: '/wp-admin/admin-ajax.php',
                          data: data,
                          contentType: false,
                          cache: false,
                          processData: false,
                          success: function (response, textStatus, jqXHR) {
                          jQuery('#EM_FORM').html(response);
                                        }
                                    });

                        }
                }
                function countWords(){
                    string = '';
                   // validateForm();
                    jQuery('.EM_count').each(function(){
                        if (jQuery(this).val()){
                            string = string +  ' '+jQuery(this).val();
                        }
                    });
                    if (string) {
                        jQuery('#EM_chars').html(string.toString().length);
                        jQuery('#EM_words').html(string.match(/\S+?\s|\S+?$/g).length);
                    }

                }

                countWords();
                jQuery('.EM_count').change(function (){
                    countWords();
                    validateForm();
                });
                jQuery('#EM_next').click(function (){
                    nextStep();
                    return false;
                });
        </script>