<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var_dump($pagination);
$adUrl = get_option('EM_adUrl');
$catUrl='';
foreach ($categories as $key => $value) {
    if ( isset($catUrl) and $key ==(int)$_GET['catUrl']) {
        $catUrl ='?catUrl='.$key;
        $name=$value;
    }
}
?>
 <div class="content col-md-9 col-sm-12">
<!--    <ol class="breadcrumb">
      <li><a href="/"><?= __('Номе', 'em-plugin')?></a></li>
        <?php if ($catUrl): ?>
            <li><a href="/<?=$pageUrl?><?=$catUrl?>"><?= $name?></a></li>
        <?php else: ?>
            <li class="active"><?= __('Ads', 'em-plugin')?></li>
        <?php endif; ?>


    </ol>-->
    <h1 class="bussines-title "><?= __('Ads', 'em-plugin')?></h1>
    <div class="formad">
    	<form  method="GET" action="/<?=get_option('EM_searchUrl')?>" class="form-inline">
    		<div class="form-group" id="simple_search">
    			<!--<a href="#" class="btn btn_editad">Редактировать объявление</a>-->
    			<div class="input-group search">
	                <input type="text" value="<?=(isset($searchParam['searchStr']))?$searchParam['searchStr']:''?>" name="searchParam[searchStr]" class="form-control" placeholder="<?= __('Search', 'em-plugin')?>...">
	                <span class="input-group-btn">
	                  <button class="btn btn-default" type="submit"><span class="icon-search"></span></button>
	            	</span>
	            </div>
	            <a href="javascript:void(0)" onclick="showHide('advant_search')" class="advant_search"><?= __('Advanced Search', 'em-plugin')?></a>
    		</div>
    		<div id="advant_search">
			  <div class="form-group">
			  	<select name="searchParam[categoryId]" class="form-control classy_cat">
                    <option value="0"><?= __('All categories', 'em-plugin')?></option>
                    <?php foreach ($categories as $id=>$name ): ?>
                    <option <?=(isset($searchParam['categoryId']) and $searchParam['categoryId'] == $id)?'selected=""':'' ?>  value="<?= $id?>"><?= $name?></option>
                    <?php endforeach; ?>
			  	</select>
			    <label for="price_max"><?= __('Price', 'em-plugin')?>:</label>
			    <input type="text"  value="<?=(isset($searchParam['priceMin']))?$searchParam['priceMin']:''?>" name="searchParam[priceMin]" class="form-control" id="price_min" placeholder="Min">
			    <input type="text"  value="<?=(isset($searchParam['priceMax']))?$searchParam['priceMax']:''?>" name="searchParam[priceMax]" class="form-control" id="price_max" placeholder="Max">
			  </div>
			  <div class="form-group">
			    <input type="text" value="<?=(isset($searchParam['State']))?$searchParam['State']:''?>" name="searchParam[State]" class="form-control" id="state" placeholder="<?= __('State', 'em-plugin')?>">
			    <input type="text" value="<?=(isset($searchParam['City']))?$searchParam['City']:''?>" name="searchParam[City]" class="form-control" id="sity" placeholder="<?= __('City', 'em-plugin')?>">
				<a href="/<?=get_option('EM_searchUrl')?>" class="clearform"><?= __('Clear form', 'em-plugin')?></a>
				<button type="submit" class="btn btn-blue7d"></button>
			  </div>
			</div>
		</form>
    </div>
    <?php if ($ads): ?>

    <?php foreach ($ads as $ad ): ?>
    <div class="classy-item ">
        <a href="/<?= $adUrl?>?catUrl=<?= $ad['categoryId']?>&adId=<?= $ad['id']?>"><h4><?= $ad['user']?></h4></a>
        <data><?= date('d M h:i',$ad['date'])?></data>
      <!--<div class="view"><span class="icon-eye"></span> 123</div>-->
        <?= $ad['content']?>
    </div>
    <?php endforeach; ?>
    <?php else: ?>
    <div class="EM_nothing">
       <?= __('Unfortunately, nothing found', 'em-plugin')?>
    </div>
    <?php endif; ?>

    <div>
        <?php if (count($pagination) >1): ?>
            <nav>
             <ul class="pagination">
                <li class="<?php if ($page<2): ?>disabled<?php endif; ?>">
                    <a href="<?= $basePaginationUrl?><?=($page>1)?$page-1:$page?>" aria-label="Previous">
                    <?= __('Prev', 'em-plugin')?>
                  </a>
                </li>
                 <?php foreach ($pagination as $pageNum): ?>

                    <li  class="<?php if ($page == $pageNum): ?>active<?php endif; ?>"><a href="<?= $basePaginationUrl?><?=$pageNum ?>"><?=$pageNum ?></a></li>
                 <?php endforeach; ?>
                <li class="<?php if ($page == count($pagination) ): ?>disabled<?php endif; ?>">
                    <a href="<?= $basePaginationUrl?><?=($page< count($pagination))?$page+1:$page?>" aria-label="Next">
                      <?= __('Next', 'em-plugin')?>
                    </a>
                </li>
             </ul>
           </nav>
        <?php endif; ?>

   </div>
  </div>
