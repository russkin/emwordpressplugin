<div class="ads-list">

<?php if (is_array($ads)): ?>
<?php foreach ($ads as $key => $ad): ?>
    <div class="classy-item">
        <a href="<?php echo $_SERVER['REQUEST_URI']; ?>?n=<?php echo $ad['id']; ?>"><h4><?php echo $ad['FIELDS'][2]['value']; ?></h4></a>
	<data><?php echo date('m.d.Y', $ad['start_date'] ); ?></data>
	<div class="view"><span class="icon-eye"></span> 123</div>
	<p><?php echo mb_substr( $ad['FIELDS'][3]['value'], 0, 238, 'UTF-8' ); ?>...</p>
    </div>
<?php endforeach; ?>
<?php else: 
    echo $ads;
endif; ?>
</div>