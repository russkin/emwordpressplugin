<?php

/**
 * View для вывода первого шага мастера подачи обьявления
 */

?>
<div id="EMadvertiseAdd" class="content col-md-9 col-sm-12">
<!--    <ol class="breadcrumb">
        <li><a href="/"> <?= __('Home', 'em-plugin')?></a></li>
      <li><?= __('Ads', 'em-plugin')?></li>
      <li class="active"><?= __('Place an ad', 'em-plugin')?></li>
    </ol>-->
    <h1 class="bussines-title "><?= __('Place an ad', 'em-plugin')?>
        <!--<a id="EM_edit_ad" href="#" class="edit_ad"><span class="glyphicon glyphicon-edit"></span><?= __('Edit ad', 'em-plugin')?></a>-->
        </h1>
    <div style="width: 600px;">
        <div id="EM_adv">
            <div class="steps">
      <div class="progress">
          <div class="progress-bar" id="EM_progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="">
          <span class="sr-only">1 <?= __('step', 'em-plugin')?></span>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">1</div>
        <div class=" em_step em_step1 text-center"><?= __('Filling out the form feed ads', 'em-plugin')?></div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">2</div>
        <div class=" em_step em_step2 text-center disabled"><?= __('Site selection for advertising', 'em-plugin')?></div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">3</div>
        <div class=" em_step em_step3 text-center disabled"><?= __('Preview and publish (payment)', 'em-plugin')?></div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">4</div>
        <div class=" em_step em_step3 text-center disabled"><?= __('View ads and verification', 'em-plugin')?></div>
      </div>

      <div class="form_classyadd">
        <div class="form_classyadd step1" id="step1">
          <h3><?= __('Fill the form', 'em-plugin')?></h3>
        </div>
          <form id="EM_FORM" enctype="multipart/form-data" class="form-horizontal">
            <input type="hidden" name='EM_Step' value='2' />
            <input type="hidden" name='action' value='EM_action' />
            <input type="hidden" name='AdvertiseUser[advertise_id]' value='<?=$adContent['id']?>' />
          <div class="form-group">
            <label for="category" class="col-sm-2 control-label"><?= __('Category', 'em-plugin')?>
                <span class="required">*</span>
                <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=__('Specify the category ad placement', 'em-plugin')?>">?</span></label>
            <div class="col-sm-10">
              <select class="form-control required_" id="AdvertiseUser_category_id" name="AdvertiseUser[category_id]">
                  <option value="0" class="disabled"><?=__('Select category', 'em-plugin')?></option>
                    <?php foreach ($adCategoryes as $id => $name ): ?>
                    <option <?=(isset($_SESSION['EM_advFormValue']['AdvertiseUser']['category_id']) and $id == $_SESSION['EM_advFormValue']['AdvertiseUser']['category_id'])?'selected=""':'' ?> value="<?= $id?>"><?= $name?></option>
                    <?php endforeach; ?>
              </select>
            </div>
          </div>

            <?php foreach ($adContent['fields'] as $field ): ?>
                <?php if ($field['name'] == 'Content' and $field['type'] == 'textarea'): ?>
                    <div class="form-group">
                        <label for="classytext" class="col-sm-2 control-label "><?=$field['name'] ?>
                            <span class="required">*</span> <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=$field['description'] ?>">?</span></label>
                      <div class="col-sm-10">
                        <textarea  class="form-control required_ EM_count" name="field[<?=$field['id']?>]" id="classytext"><?=(isset($_SESSION['EM_advFormValue']['AdvertiseUser']['field'][$field['id']]))?$_SESSION['EM_advFormValue']['AdvertiseUser']['field'][$field['id']]:""?></textarea>
                        <div class="classytext_counters">
                            <div class="counter"><?=__('Сharacters', 'em-plugin')?>: <span id="EM_chars"></span></div>
                          <div class="counter"><?=__('Words', 'em-plugin')?>: <span id="EM_words"></span></div>
                        </div>
                        <p><?=__('Items marked', 'em-plugin')?> <span class="required">*</span>, <?=__('required', 'em-plugin')?>.</p>
                      </div>
                    </div>
                <?php endif; ?>
                <?php if (in_array($field['type'], array('text', 'email', 'number', 'tel', 'checkbox', 'date','file' ))): ?>
                    <div class="form-group">
                      <label for="field_<?=$field['id']?>" class="col-sm-2 control-label"><?=$field['name'] ?><?php if ($field['requre'] == 1): ?> <span class="required">*</span> <?php endif; ?> <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=$field['description'] ?>">?</span></label>
                      <div class="col-sm-10">
                            <?php $value = (isset($_SESSION['EM_advFormValue']['AdvertiseUser']['field'][$field['id']]))?$_SESSION['EM_advFormValue']['AdvertiseUser']['field'][$field['id']]:null;          ?>
                          <input class='<?php if ( !in_array($field['type'], ['checkbox','file'])): ?>EM_count<?php endif; ?> form-control  <?php if ($field['requre'] == 1): ?>required_ <?php endif; ?>' id="field_<?=$field['id']?>"  type="<?= $field['type'] ?>"
                                   name="<?=($field['type']=='file')?'field_'.$field['id'].'':'field['.$field['id'].']'?>"
                               <?php if ($field['type'] == 'checkbox' and $value): ?>
                                   checked=""
                                   <?php endif; ?>
                               <?php if ($field['type'] != 'checkbox'): ?>
                                   value="<?= $value ?>"

                               <?php else: ?>
                                   value="1"
                               <?php endif; ?>
                               <?php if ($field['type'] == 'number'): ?>
                                   step="any"
                               <?php endif; ?>
                               <?php if ($field['requre'] == 1): ?>
                                   required="required"
                               <?php endif; ?>
								   title="<?=($field['description'])?$field['description']:$field['name'] ?>"
                               />
                        </div>
                    </div>
                <?php endif; ?>
                <?php if ($field['type']=='textarea' and $field['name'] !='Content'): ?>
                    <div class="form-group">
                        <label for="classytext" class="col-sm-2 control-label "><?=$field['name'] ?>
                            <?php if ($field['requre'] == 1): ?> <span class="required">*</span> <?php endif; ?>  <span class="badge" data-toggle="tooltip" data-placement="right" title="<?=$field['description'] ?>">?</span></label>
                      <div class="col-sm-10">
                        <textarea  class="form-control <?php if ($field['requre'] == 1): ?>required_ <?php endif; ?> EM_count" name="field[<?=$field['id']?>]" id="classytext"><?=(isset($_SESSION['EM_advFormValue']['AdvertiseUser']['field'][$field['id']]))?$_SESSION['EM_advFormValue']['AdvertiseUser']['field'][$field['id']]:""?></textarea>

                      </div>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
            <div id="EM_alert" class="alert alert-danger hidden" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                 <strong><?= __('Correct the form errors', 'em-plugin')?></strong>
                <ul id="EM_errors" class=""></ul>
            </div>
          <div class="form-group">
            <div class="col-sm-12">
                <a href="" id="EM_prev" class="btn btn-default disabled"><?= __('Prev', 'em-plugin')?></a>
                <a href="#" id="EM_next" onclick="" class="btn btn-primary pull-right"><?= __('Next', 'em-plugin')?></a>
            </div>
          </div>
            <script type="text/javascript">
                var EM_errors='';
                jQuery('#EM_progress-bar').css('width','10%');
                jQuery('#EM_step').addClass('disabled');
                jQuery('#EM_step1').removeClass('disabled');
                function validateForm(){
                    EM_errors='';
                    jQuery('.required_').removeClass('requiredBorder');
                    jQuery('.required_').each(function(){
                        if (! jQuery(this).val() || jQuery(this).val()==0 ) {
                            EM_errors=EM_errors+"<li><?= __('Blank value', 'em-plugin')?> </li>";
                            jQuery(this).addClass('requiredBorder');
                        }
                    });
                    jQuery('.EM_count').each(function(){
                        if (cFM_checkFullness(jQuery(this))) {
                            EM_errors=EM_errors+"<li><?= __('Wrong value', 'em-plugin')?> </li>";
                            jQuery(this).addClass('requiredBorder');
                        }
                    });
                    return EM_errors;
                }

                function cFM_checkFullness(obj){
                    var error ;
                    switch(obj.attr('type')){
                        case 'email':
                            var regCheck = new RegExp("^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
                            if(!regCheck.test(obj.val())){
                                error='wrong';
                            }
                            break;
                        case 'num':
                            var regCheck = new RegExp('[^0-9\s-]+');
                            if(regCheck.test(obj.val()))
                            error='wrong';
                            break;
                    }
                    return error;
                }


                function nextStep(){
                    errors=validateForm();
                    console.log(errors);
                    if (errors) {
                        jQuery('#EM_errors').html(errors);
                        jQuery('#EM_alert').removeClass('hidden');
                        EM_errors = '';
                    }else{
                            form = jQuery('#EM_FORM').get(0);
                            data=new FormData(form);
                            jQuery.ajax({
                              type: 'POST',
                              url: '/wp-admin/admin-ajax.php',
                              data: data,
                              contentType: false,
                              cache: false,
                              processData: false,
                              success: function (response, textStatus, jqXHR) {
                              jQuery('#EM_FORM').html(response);
                                            }
                                        });

                        }
                }
                function countWords(){
                    string = '';
                   // validateForm();
                    jQuery('.EM_count').each(function(){
                        string = string +  ' '+jQuery(this).val();
                    });
                    if (string) {
                        jQuery('#EM_chars').html(string.toString().length);
                        jQuery('#EM_words').html(string.match(/\S+?\s|\S+?$/g).length);
                    }

                }
           //     countWords();
                jQuery('.EM_count').change(function (){
                    countWords();
                    validateForm();
                });
                jQuery('#EM_next').click(function (){
                    nextStep();
                    return false;
                });
            </script>
        </form>




      </div>

    </div>
        </div>
    </div>
    </div>
<style>
    .requiredBorder{
        border-width: 1px!important;
        border-color: red!important;
    }
</style>
