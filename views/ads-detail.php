<div class="ads-detail">
    <h2><?php echo $ads['Название']; ?></h2>
    <div class="bussines-address">
        <address>
            <a href="#">
                <img src="<?php echo plugins_url('EMClient/img/pointer.png')?>">
                <?php echo $ads['Адрес']; ?>
            </a>
        </address>
    </div>
    <div class="bussines-phones">
        <img src="<?php echo plugins_url('EMClient/img/phone.png')?>">
        <phone><?php echo $ads['Телефон']; ?></phone>
    </div>
    <div class="ads-description">
        <?php echo $ads['Описание объявления']; ?>
    </div>
    <div class="ads-cat">
        <strong>Категория:</strong> <a href="/<?php  echo $page; ?>/<?php  echo $category['id']; ?>/"><?php  echo $category['name']; ?></a>
    </div>
</div>
