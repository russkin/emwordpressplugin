<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var_dump($_SESSION['EM_advFormValue']['addSites']);
?>
<input type="hidden" name='action' value='EM_action' />
<input type="hidden" id="EM_Step" name='EM_Step' value='3' />
<input type="hidden" name='AdvertiseUser[advertise_id]' value='<?=$adContent['id']?>' />
<div class="form_classyadd step2" id="step2">
  <h3><?= __('Place', 'em-plugin')?></h3>
    <div class="newspaper">
        <div class="checkbox">
            <label>
                <input type="checkbox"  checked="checked"/> <strong><?= (isset($adContent['site']))?$adContent['site']['name']:''?></strong>
            </label>
        </div>
        <table class="table table-hover">
            <tr><td><label><input type="checkbox" checked="checked"> <?= __('Ad in ', 'em-plugin')?> <?= (isset($adContent['site']))?$adContent['site']['name']:''?>
                    </label></td><td>$<?=$adContent['price'] ?>
                    <?=(isset($adContent['site']) and $adContent['site']['type']=='Web site')?__('For 1 ads at one week', 'em-plugin'):__('For 1 words ads at 1 issue', 'em-plugin')?></td></tr>
            <!--перебираем сервисы-->
            <?php foreach ($adContent['servises'] as $servises): ?>
                <?php if ($servises['siteId'] == $adContent['site']['id']): ?>
                        <tr>
                            <td><label>
                                    <?php if ($servises['type']=='checkbox'): ?>
                                    <input type="hidden" name="servises[<?= $servises['id'] ?>]" value="0">
                                    <input type="checkbox"  name="servises[<?= $servises['id'] ?>]" value="1"

                                    <?=(isset($_SESSION['EM_advFormValue']['servises'][$servises['id']]) and $_SESSION['EM_advFormValue']['servises'][$servises['id']])?'checked=""':'' ?>
                                     > <?= $servises['name'] ?>
                                    <?php else:?>
                                    <input type="hidden" name="servises[<?= $servises['id'] ?>]" value="0">
                                    <input type="checkbox"  name="servises[<?= $servises['id'] ?>]" value="1"

                                    <?=(isset($_SESSION['EM_advFormValue']['servises'][$servises['id']]) and $_SESSION['EM_advFormValue']['servises'][$servises['id']])?'checked=""':'' ?>
                                        >  <?= $servises['name'] ?><br>
                                        <input type="text"  name="value[<?= $servises['id'] ?>]" value="<?=(isset($_SESSION['EM_advFormValue']['value'][$servises['id']]) and $_SESSION['EM_advFormValue']['value'][$servises['id']])?$_SESSION['EM_advFormValue']['value'][$servises['id']]:'' ?>">
                                    <?php endif; ?>
                                </label>
                            </td>
                            <td>
                                $<?= $servises['price'] ?> <?=(isset($adContent['site']) and $adContent['site']['type']=='Web site')?__('For 1 ads at one week', 'em-plugin'):__('For 1 words ads at 1 issue', 'em-plugin')?>
                            </td>
                        </tr>
                <?php endif; ?>
            <?php endforeach; ?>
            <!--даты-->
            <?php
            $site = $adContent['site'];
            ?>
            <tr>
                <td>
                    <label>
                        <?= __('Week', 'em-plugin')?>
                        <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="<?= __('Week', 'em-plugin')?>"></span>
                    </label>
                </td>
                <td>
                    <input type="number" id='start_date' title='' name='AdvertiseUser[start_date]' min='<?= date('W')?>'
                               value='<?= (isset($_SESSION['EM_advFormValue']['AdvertiseUser']['start_date']))?$_SESSION['EM_advFormValue']['AdvertiseUser']['start_date']:date('W')+1?>'  />
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <?= __('Year', 'em-plugin')?>
                        <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="<?= __('Year', 'em-plugin')?>"></span>
                    </label>
                </td>
                <td>
                    <input type="number" id='year' title='' name='AdvertiseUser[year]' min='<?= date('Y')?>'
                               value='<?= (isset($_SESSION['EM_advFormValue']['AdvertiseUser']['year']))?$_SESSION['EM_advFormValue']['AdvertiseUser']['year']:date('Y')?>'  />
                </td>
            </tr>
            <tr>
                <td>
                    <label>

                        <?= ($site['type']=='Web site')? __('Quantity of weeks', 'em-plugin'):__('Quantity of issues', 'em-plugin')?>
                        <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="<?= ($site['type']=='Web site')? __('Quantity of weeks', 'em-plugin'):__('Quantity of issues', 'em-plugin')?>"></span>
                    </label>
                </td>
                <td>
                    <?php if ($site['type']=='Web site'): ?>
                        <input type="number" id='week' title='' name='AdvertiseUser[weeks]' min='1' max='53'
                               value='<?= (isset($_SESSION['EM_advFormValue']['AdvertiseUser']['weeks']))?$_SESSION['EM_advFormValue']['AdvertiseUser']['weeks']:1?>'  />
                    <?php else:?>
                        <input type="number" id='issues' title='' name='AdvertiseUser[issues]' min='1'
                               value='<?= (isset($_SESSION['EM_advFormValue']['AdvertiseUser']['issues']))?$_SESSION['EM_advFormValue']['AdvertiseUser']['issues']:1?>'  />
                    <?php endif; ?>

                </td>
            </tr>
<!--					<tr>
                <td colspan="2">
                    <h4>Пояснительный текст и правила</h4>
                    <p>
                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                    </p>
                </td>
            </tr>-->
        </table>
    </div>
  <?php if ($adContent['sites']): ?>




        <div class="newspaper">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                      <a id="" class="collapsed addsite" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseNewspaper" aria-expanded="false" aria-controls="collapseOne">
                        <i  class="glyphicon glyphicon-unchecked addsite_i" onclick="this.className = (this.className == 'glyphicon glyphicon-unchecked' ? 'glyphicon glyphicon-check' : 'glyphicon glyphicon-unchecked')"><span style="font-family: Open Sans"><?= __('Post ads on other sites', 'em-plugin')?></span></i>
                    </a>
                  </h4>
                </div>
                <div id="collapseNewspaper" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <?php foreach ($adContent['sites'] as $site): ?>
                        <div class="panel-body">
                          <table class="table table-hover">
                              <tr><td><label>
                                      <input type="hidden" name="addSites[<?= $site['id']?>][on]" value="0">
                                      <input name="addSites[<?= $site['id']?>][on]"
                                             <?=(isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['on']) and $_SESSION['EM_advFormValue']['addSites'][$site['id']]['on'])?'checked=""':'' ?>
                                             type="checkbox"> <?= __('Ad in ', 'em-plugin')?> <?= $site['name']?></label>
                                  </td>
                                  <td>
                                      $<?=$site['price']?>
                                      <?=($site['type']=='Web site')?__('For 1 ads at one week', 'em-plugin'):__('For 1 words ads at 1 issue', 'em-plugin')?>
                                  </td>
                              </tr>
                            <!--перебираем сервисы-->
                            <?php foreach ($adContent['servises'] as $servises): ?>
                                <?php if ($servises['siteId'] == $site['id']): ?>
                                        <tr>
                                            <td><label>
                                                    <?php if ($servises['type']=='checkbox'): ?>
                                                    <input type="hidden" name="addSites[<?= $site['id']?>][servises][<?= $servises['id'] ?>]" value="0">
                                                    <input type="checkbox"  name="addSites[<?= $site['id']?>][servises][<?= $servises['id'] ?>]" value="1"

                                                    <?=(isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['servises'][$servises['id']]) and $_SESSION['EM_advFormValue']['addSites'][$site['id']]['servises'][$servises['id']])?'checked=""':'' ?>
                                                     > <?= $servises['name'] ?>
                                                    <?php else:?>
                                                   <input type="hidden" name="addSites[<?= $site['id']?>][servises][<?= $servises['id'] ?>]" value="0">
                                                    <input type="checkbox"  name="addSites[<?= $site['id']?>][servises][<?= $servises['id'] ?>]" value="1"

                                                    <?=(isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['servises'][$servises['id']]) and $_SESSION['EM_advFormValue']['addSites'][$site['id']]['servises'][$servises['id']])?'checked=""':'' ?>
                                                     >
                                                        <?= $servises['name'] ?><br>
                                                        <input type="text"  name="addSites[<?= $site['id']?>][values][<?= $servises['id'] ?>]" value="<?=(isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['values'][$servises['id']]) and $_SESSION['EM_advFormValue']['addSites'][$site['id']]['values'][$servises['id']])?$_SESSION['EM_advFormValue']['addSites'][$site['id']]['values'][$servises['id']]:'' ?>">
                                                    <?php endif; ?>
                                                </label>
                                            </td>
                                            <td>
                                                $<?= $servises['price'] ?> <?=(isset($site['type']) and $site['type']=='Web site')?__('For 1 ads at one week', 'em-plugin'):__('For 1 words ads at 1 issue', 'em-plugin')?>
                                            </td>
                                        </tr>
                                <?php endif; ?>
                            <?php endforeach; ?>
                            <tr>
                                <td>
                                    <label>
                                        <?= __('Week', 'em-plugin')?>
                                        <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="<?= __('Week', 'em-plugin')?>"></span>
                                    </label>
                                </td>
                                <td>
                                    <input type="number" id='start_date_<?= $site['id']?>' title='' name='addSites[<?= $site['id']?>][start_date]' min='<?= date('W')?>'
                                               value='<?= (isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['start_date']))?$_SESSION['EM_advFormValue']['addSites'][$site['id']]['start_date']:date('W')+1?>'  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>
                                        <?= __('Year', 'em-plugin')?>
                                        <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="<?= __('Year', 'em-plugin')?>"></span>
                                    </label>
                                </td>
                                <td>
                                    <input type="number" id='year_<?= $site['id']?>' title='' name='addSites[<?= $site['id']?>][year]' min='<?= date('Y')?>'
                                               value='<?= (isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['year']))?$_SESSION['EM_advFormValue']['addSites'][$site['id']]['year']:date('Y')?>'  />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>

                                        <?= ($site['type']=='Web site')? __('Quantity of weeks', 'em-plugin'):__('Quantity of issues', 'em-plugin')?>
                                        <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="top" title="<?= ($site['type']=='Web site')? __('Quantity of weeks', 'em-plugin'):__('Quantity of issues', 'em-plugin')?>"></span>
                                    </label>
                                </td>
                                <td>
                                    <?php if ($site['type']=='Web site'): ?>
                                        <input type="number" id='week_<?= $site['id']?>' title='' name='addSites[<?= $site['id']?>][weeks]' min='1' max='53'
                                               value='<?= (isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['weeks']))?$_SESSION['EM_advFormValue']['addSites'][$site['id']]['weeks']:1?>'  />
                                    <?php else:?>
                                        <input type="number" id='issues_<?= $site['id']?>' title='' name='addSites[<?= $site['id']?>][issues]' min='1'
                                               value='<?= (isset($_SESSION['EM_advFormValue']['addSites'][$site['id']]['issues']))?$_SESSION['EM_advFormValue']['addSites'][$site['id']]['issues']:1?>'  />
                                    <?php endif; ?>

                                </td>
                            </tr>
<!--                              <tr><td colspan="2">
                                  <h4>Пояснительный текст и правила</h4>
                                  <p>
                                      Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                  </p>
                              </td></tr>-->
                          </table>
                        </div>
                    <?php endforeach; ?>
                </div>
              </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <div class="col-sm-12">
        <a href="#" id="EM_prev" class="btn btn-default "><?= __('Prev', 'em-plugin')?></a>
        <a href="#" id="EM_next"  class="btn btn-primary pull-right"><?= __('Next', 'em-plugin')?></a>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery('#EM_progress-bar').css('width','35%');
    jQuery('.em_step').addClass('disabled');
    jQuery('.em_step2').removeClass('disabled');
    jQuery('#EM_next').click(function (){
        nextStep();
        return false;
    });
    jQuery('#EM_prev').click(function (){
        step = parseInt(jQuery('#EM_Step').val())-2;
        jQuery('#EM_Step').val(step);
        nextStep();
        return false;
    });

    function nextStep(){

            data=jQuery('#EM_FORM').serialize();
            jQuery.post( "/wp-admin/admin-ajax.php", data)
                .done(function( data ) {
                    jQuery('#EM_FORM').html(data);
                })
                .fail(function() {
                alert( "error!" );
                });

    }
<?php if (isset($_SESSION['EM_advFormValue']['addSites'])): ?>
jQuery('.addsite').click();
jQuery('.addsite_i').removeClass('glyphicon-unchecked');
jQuery('.addsite_i').addClass('glyphicon-check');
<?php endif; ?>
</script>


