<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//var_dump($params);
?>
<input type="hidden" name='action' value='EM_action' />
<input type="hidden" id="EM_Step" name='EM_Step' value='5' />
<input type="hidden" name='advId' value='<?=$params['adId']?>' />
<input type="hidden" name='invoiceId' value="" id="invoiceId">
<input type="hidden" name="payOrder" value="1"/>
<input type="hidden" name="AutorizeForm[AMOUNT]" value="<?=$params['price']?>"/>
      <div class="form_classyadd step3" id="step3">
<!--          <h3>Введите код активации <i style="font-size: 0.5em;">(отправляеться по email)</i> </h3>
          <input type="text">-->
          <hr>

			  <div class="form-group">
			    <label for="inputCountry" class="col-sm-2 control-label"><?= __('Country', 'em-plugin')?></label>
			    <div class="col-sm-10">
			      <input type="text" name="AutorizeForm[BSTATE]" class="form-control" id="inputCountry" placeholder="<?= __('Country', 'em-plugin')?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputcard" class="col-sm-2 control-label"><?= __('Credit card', 'em-plugin')?></label>
			    <div class="col-sm-10">
			      <input type="text" name="AutorizeForm[CC]" class="form-control" id="inputcard" placeholder="<?= __('Credit card', 'em-plugin')?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputExpiration" class="col-sm-2 control-label"><?= __('Expiration', 'em-plugin')?></label>
			    <div class="col-sm-10">
                    <input type="number"  name="AutorizeForm[EXPMNTH]" value="<?= date('m')?>" id="archform-expmonth" placeholder="<?= date('m')?>" class=""  min="1" max="12" style="width: 60px;">
                    <input type="number" id="archform-expyear" value="<?= date('y')?>" placeholder="<?= date('y')?>" class="" name="AutorizeForm[EXPYR]" min="<?= date('y')?>" max="99" style="width: 100px;">
					<label> / <?= __('CVV2 code', 'em-plugin')?>:</label>
					<input type="text" id="archform-cvv2" class="" name="AutorizeForm[CSC]" style="width: 60px;">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputName" class="col-sm-2 control-label"><?= __('First name', 'em-plugin')?></label>
			    <div class="col-sm-10">
			      <input type="text" name="AutorizeForm[FIRSTNAME]"  class="form-control" id="inputName" placeholder="<?= __('First name', 'em-plugin')?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputLast" class="col-sm-2 control-label"><?= __('Last name', 'em-plugin')?></label>
			    <div class="col-sm-10">
			      <input type="text" name="AutorizeForm[LASTNAME]" class="form-control" id="inputLast" placeholder="<?= __('Last name', 'em-plugin')?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputStreet" class="col-sm-2 control-label"><?= __('Adress', 'em-plugin')?></label>
			    <div class="col-sm-10">
                    <input type="text" name="AutorizeForm[BADDRESS]" class="form-control" id="inputStreet" placeholder="<?= __('Adress', 'em-plugin')?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputZIP" class="col-sm-2 control-label"><?= __('ZIP code', 'em-plugin')?></label>
			    <div class="col-sm-10">
			      <input type="text" name="AutorizeForm[BZIP]" class="form-control" id="inputZIP" placeholder="<?= __('ZIP code', 'em-plugin')?>">
			    </div>
			  </div>

			<div class="form-group">
	            <div class="col-sm-12">
                    <!--<a href="#" id="EM_prev" class="btn btn-default "><?= __('Prev', 'em-plugin')?></a>-->
                    <a href="#" id="EM_next"  class="btn btn-primary pull-right"><?= __('Pay', 'em-plugin')?> $<?=$params['price']?></a>
	            </div>
	        </div>
        </div>
<script type="text/javascript">
    jQuery('#EM_progress-bar').css('width','90%');
    jQuery('.em_step').addClass('disabled');
    jQuery('.em_step4').removeClass('disabled');
    jQuery('#EM_next').click(function (){
        jQuery('#EM_Step').val('5');
         if (confirm('<?= __('Really pay', 'em-plugin')?> $<?=$params['price']?> ?')) {
            nextStep();
        }
        return false;
    });
    jQuery('#EM_prev').click(function (){
        jQuery('#EM_Step').val('3');
        nextStep();
        return false;
    });
    function nextStep(){
            data=jQuery('#EM_FORM').serialize();
            jQuery.post( "/wp-admin/admin-ajax.php", data)
                .done(function( data ) {
                    jQuery('#EM_FORM').html(data);
                })
                .fail(function() {
                alert( "error!" );
                });

    }
</script>
