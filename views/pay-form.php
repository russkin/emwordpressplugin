<script type="text/javascript">
	function getEmUrl(){
		return "/wp-admin/admin-ajax.php";
	}
</script>
<style>
#form-steps div{float:left; width:40px;height:40px;background:#b0b0b0;border:3px solid #cccccc;border-radius:50%;font-weight:bold;text-align:center;}
#form-steps .active{background:#2E2EFE;color:#ffffff;}
.tabs{display:none;}
</style>
<?php if(!$_SESSION['userEmId']): 
//echo $em_form; ?>

<div class="EM_auth_form">
	<ul id="EM_login_tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#EM_loginTAb" data-toggle="tab">Войти</a></li>
        <li><a href="#EM_RegisterTAb" data-toggle="tab">Регистрация</a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="EM_loginTAb">
            <h1>Войти</h1>
            <div class="input-group input-group-lg">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
				<input class="form-control" placeholder="login" name="UserLogin[username]" id="EM_Login" type="text" style="" autocomplete="off">	
			</div>
			<div class="input-group input-group-lg">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
				<input class="form-control" placeholder="password" name="UserLogin[password]" id="EM_Pass" type="password" style="" autocomplete="off">
			</div>
			<p class="center col-md-5">
				<a   class="btn btn-primary" id="EM_login_send" href="#">Войти</a>
			</p>
        </div>
        <div class="tab-pane" id="EM_RegisterTAb">
            <h1>Регистрация</h1>
            <div class="input-group input-group-lg">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
				<input class="form-control" placeholder="Email" name="UserLogin[username]" id="EM_LoginN" type="text" style="" autocomplete="off">	
			</div>
			<div class="input-group input-group-lg">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
				<input class="form-control" placeholder="password" name="UserLogin[password]" id="EM_PassN" type="password" style="" autocomplete="off">
			</div>
			<div class="input-group input-group-lg">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
				<input class="form-control" placeholder="Confirm password" name="UserLogin[password]" id="EM_ConfN" type="password" style="" autocomplete="off">
			</div>
			<p class="center col-md-5">
				<a   class="btn btn-primary" id="EM_Register_send" href="#">Регистрация</a>
			</p>
        </div>
    </div>
	<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#EM_login_tabs').tab();
		$('#EM_login_send').click(function(){
			vars={
				login    : $('#EM_Login').val(),
				password : $('#EM_Pass').val(),
				action   : 'EM_action',
				auth     : 'login',
				url      : location.href
			};
			jQuery.ajax({
				url: getEmUrl(),
				data: vars,
				type: "POST",
				timeout: 10000,
				success: function (data) {
					 location.reload();
//						jQuery('#EM_adv').html(data);
									},
				error: function (xhr, ajaxOptions, thrownError) {
				}
			});
			return false;
		});
		$('#EM_Register_send').click(function(){
			vars={
				login    : $('#EM_LoginN').val(),
				password : $('#EM_PassN').val(),
				Confirmpassword : $('#EM_PassN').val(),
				action   : 'EM_action',
				auth     : 'register',
				url      : location.href
			};
			jQuery.ajax({
				url: getEmUrl(),
				data: vars,
				type: "POST",
				timeout: 10000,
				success: function (data) {
//					 location.reload();
//						jQuery('#EM_adv').html(data);
									},
				error: function (xhr, ajaxOptions, thrownError) {
				}
			});
			return false;
		});

    });
</script> 




<?php else: ?>
<div class="steps">
      <div class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
          <span class="sr-only">1 step</span>
        </div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">1</div>
        <div class="text-center" id="text-center1">Заполнение формы подачи объявления</div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">2</div>
        <div class="text-center" id="text-center2">Выбор площадки для подачи объявления</div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">3</div>
        <div class="text-center" id="text-center3">Просмотр и публикация (оплата)</div>
      </div>
      <div class="col-md-3 col-sm-3 col-xs-3">
        <div class="step">4</div>
        <div class="text-center" id="text-center4">Просмотр объявления и верификация</div>
      </div>
    
<div style="clear:both;"></div>
<div id="adv_form">
</div>	
<form id="first-form" class="form-horizontal" method="post" action="#">
	<div class="tabs" id="tab-1">            
		<h3>Заполните форму</h3>				
				<input type="hidden" name="action" value="EM_action" />
				<input id="create_banner" type="hidden" value="0" name="create_banner">
				<input id="AdvertiseUser_id" type="hidden" name="AdvertiseUser[id]">
				<input name="AdvertiseUser[advertise_id]" id="AdvertiseUser_advertise_id" type="hidden" value="<?php echo $_SESSION['EM_advForm']; ?>" />
				<input type="hidden" name="sendForm" value="1"/>


<?php foreach($em_form['fields'] as $val):?>
    <?php 
    if ($val['requre'] === '1'){
        $required = 'required ';
        $req_flag = '<span class="required">*</span>';
    } else {
        $required = '';
        $req_flag = '';
    }
    ?>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="field<?php echo $val['id']; ?>"><?php echo $val['name']; ?>
        <?php echo $req_flag; ?>
        <span class="badge" title="" data-placement="right" data-toggle="tooltip" data-original-title="<?php echo $val['description'];?>">?</span>
        </label>
        <div class="col-sm-10">
        <?php if($val['type'] == "textarea"):?>
            <textarea class='form-control' <?php echo $required; ?>name="field[<?php echo $val['id']; ?>]"></textarea>
        <?php else: ?>
            <input type="<?php echo $val['type']; ?>" <?php echo $required; ?>class='form-control' name="field[<?php echo $val['id']; ?>]" value="<?php echo $val['value']; ?>">
        <?php endif; ?>
        </div>
    </div>
<?php endforeach; ?>
      
    <div class="form-group">
	<label class="col-sm-2 control-label" for="AdvertiseUser_category_id">
            Категории объявлений
            <span class="required">*</span>
            <span class="badge" title="" data-placement="right" data-toggle="tooltip" data-original-title="">?</span>
        </label>
        <div class="col-sm-10">
            <select class='form-control' required id="AdvertiseUser_category_id" name="AdvertiseUser[category_id]">
            <?php foreach ($category as $key => $val): ?>
            <option value="<?php echo $key;?>"><?php echo $val;?></option>
            <?php endforeach; ?>
            </select>
        </div>
    </div>
					<div class="form-actions">
						<a id="toStep2" class="btn btn-primary pull-right" href="#">Дальше </a>
					</div>
                                
	</div>
	
	
	<div class="tabs" id="tab-2">
            <h3>Заполните форму</h3>
            <div class="form-group">
                <div class="date-block">
                    <div class="date-label"><label for="AdvertiseUser_start_date">Неделя</label></div><br/>
                    <input class="form-control" id="start_date" type="number" value="38" max="53" min="1" name="AdvertiseUser[start_date]" title="">
                </div>
                <div class="date-block">
                    <div class="date-label"><label for="AdvertiseUser_year">Year</label></div><br/>
                    <input id="year" class="form-control" type="number" value="<?php echo date('Y');?>" max="" min="<?php echo date('Y');?>" name="AdvertiseUser[year]" title="">
                </div>
                <div class="date-block">
                    <div class="date-label"><label for="AdvertiseUser_year">Недель размещения</label></div><br/>
                    <input type="number" class="form-control" name="AdvertiseUser[weeks]" id="week" value="1" />
                </div>
            </div>
            
            <input type="hidden" name="action" value="EM_action" />
            <div class="col-sm-12">
                <span id="s_date"></span>
                <span id="e_date"></span>
            </div>
		<div id="paper" style="display:none;">
			<div class="pull-left control-group" rel="paper">
				<label class="control-label" for="AdvertiseUser_issues">Выпуски</label>
				<div class="controls">
					<input min="1" style="width:50px;" name="AdvertiseUser[issues]" id="AdvertiseUser_issues" type="number">
					<p id="AdvertiseUser_issues_em_" style="display:none" class="help-block"></p>
				</div>
			</div>
		</div>
	
	<?php foreach($em_form['servises'] as $serv):?>
        <div class="form-group">            
            <input id="s_<?php echo $serv['id']; ?>" class="servise" type="<?php echo $serv['type']; ?>" name="servises[<?php echo $serv['id']; ?>]" title="<?php echo $serv['name']; ?>" rel="serv">
            <label class="col-sm-2 control-label" for="s_<?php echo $serv['id']; ?>"><?php echo $serv['name']; ?> ($<?php echo $serv['price']; ?>)</label>
        </div>
	<?php endforeach; ?>

<div class="amount">
<h1>
Сумма:
<span id="AMT" style="color:red"> </span>
</h1>
</div>
	<div class="form-group">
            <div class="col-sm-12">
                <a id="backFirst" class="btn btn-primary" href="#">Назад</a>
		<a id="sendFirst" class="btn btn-primary pull-right" href="#">Оплатить </a>
            </div>
	</div>
</div>
</form>	
	
	
	<div class="tabs" id="tab-3">
            <h2>Предварительный просмотр объявления</h2>
            <div id="ads-preview"></div>
	<a id="toStep4" class="btn btn-primary pull-right" href="#">Далее </a>
	</div>
	<div class="tabs" id="tab-4">
		<h2>Форма оплаты кредитной картой</h2>
		<form id="paytrace-form" method="post" action="#">
		<input type="hidden" name="payOrder" value="1"/>
			<?php foreach($pay_form as $val):?>
			<label><?php echo $val['label']; ?></label><input type="<?php echo $val['type']; ?>" name="<?php echo $val['name']; ?>" value="<?php echo $val['value']; ?>"><br/>			
			<?php endforeach; ?>
			<div class="form-actions">
				<a id="EM_pay_send" class="btn btn-primary pull-right" href="#">Оплатить </a>
			</div>
		</form>
	</div>
</div>
                        <script type="text/javascript">

                            function update_price4fields() {
                                var vars = 'advertise_id=' + jQuery('#AdvertiseUser_advertise_id').val() + '&weeks=' + jQuery('#week').val() + '&issues=' + jQuery('#AdvertiseUser_issues').val();
                                vars = vars + '&year=' + jQuery('#year').val()+'&updatePrice=1&action=EM_action';
                                var content = '';
                                jQuery('.fields_adv').each(
                                        function () {
                                            if (jQuery(this).val()) {
                                                content += ' ' + jQuery(this).val();
                                            }
                                        });
                                vars += '&content=' + content;
                                jQuery('input:checkbox:checked').each(
                                        function () {
                                            if (jQuery(this).attr('rel') == 'serv') {

                                                vars += '&ids[' + jQuery(this).attr('id') + ']=' + jQuery(this).val();
                                            }
                                        });

                                jQuery('.serv_val').each(
                                        function () {
                                            vars += '&value[' + jQuery(this).attr('id') + ']=' + jQuery(this).val();
                                        });
                                jQuery.ajax({
                                    url: getEmUrl(),
                                    data: vars,
                        				dataType: "json",
                                    type: "POST",
                                    timeout: 10000,
                                    success: function (data) {
                                        jQuery('#AMT').html(data);
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                    }
                                });

                            }


                            jQuery(function () {
                                var advertise_id = jQuery('#AdvertiseUser_advertise_id').val();
                                            jQuery('.servise, .serv_val, #week, .fields_adv, #AdvertiseUser_issues').change(function () {
                                    update_price4fields();
                                });

                                jQuery('.servise').change(function () {
                                    if (jQuery(this).attr('title') == 'make banner' && jQuery(this).prop("checked")) {
                                        jQuery('#sendMail').modal('show');
                                    }
                                    if (jQuery(this).attr('title') == 'make banner' && !jQuery(this).prop("checked")) {
                                        jQuery('#email').val('');
                                    }
                                });
                            });

    //	обновляем список сайтов
        function update_site(obj) {
        }

        function prepare_selects() {
        }
    //	обновляем список языков
        function update_lang() {
        }

        function updateSiteTypes() {
        }
    //	обновляем список категорий сайта
        function update_ads_cats() {

        }
    //	обновляем список обьявлений
        function update_adv() {
        }
    //	Собираем поля и сервисы до кучи
        function make_fields_nServises() {
        }
        function update_price() {
            Advertise_id = jQuery('#AdvertiseUser_advertise_id').val();
            start_date = jQuery('#start_date').val();
            weeks = jQuery('#week').val();
            year = jQuery('#year').val();
            jQuery.ajax({
                type: 'POST',
                url: getEmUrl(),
                dataType: "json",
                data: {Advertise_id: Advertise_id, start_date: start_date, weeks: weeks, year: year,getDate:1,action: "EM_action"},
                success: function (data) {
                    jQuery('#s_date').html(data.start_date);
                    jQuery('#e_date').html(data.end_date);
                    jQuery('#s_date').show();
                    jQuery('#e_date').show();
                }
            });
        }

        function update_pricelist(adv) {
            if (!adv) {
                advertise_id = jQuery('#AdvertiseUser_advertise_id').val();
            } else {
                advertise_id = adv;
            }

            jQuery.post(getEmUrl(), {advertise_id: advertise_id,updatePricelist:1,action: "EM_action"})
                    .done(function (data) {
                        if (data) {
                            jQuery('#pricelist').show();
                            jQuery('#price_modal_body').html(data);
                        } else {
                            jQuery('#pricelist').hide();
                        }
                    })
                    .fail(function () {
                        alert("error!");
                    })
        }
		
    jQuery(function () {
        jQuery('.badge').mouseover(function(){            
            var pos = jQuery(this).position();
            jQuery(this).after('<div class="tooltip">'+jQuery(this).attr('data-original-title')+'</div>');

//            alert('left: ' + pos.left + ', top: ' + pos.top);
            jQuery('.tooltip').position({top: -jQuery(this).height(), left:  jQuery(this).width()});
//            jQuery('.tooltip').show();
        });
        jQuery('.badge').mouseout(function(){
            jQuery('.tooltip').hide();
            jQuery(this).find('.tooltip').remove();
        });
		jQuery('#tab-1').show();
                jQuery('.progress-bar').css({"width":"10%"});
                jQuery('#text-center1').css({"color":"#333"});
		jQuery('#toStep2').click(function () {
			jQuery('.tabs').hide();
			jQuery('#tab-2').show();
			jQuery('.progress-bar').css({'width':'35%'});
                        jQuery('#text-center2').css({"color":"#333"});
		});
        jQuery('#backFirst').click(function () {
            jQuery('.tabs').hide();
            jQuery('#tab-1').show();
            jQuery('.progress-bar').css({"width":"10%"});
            jQuery('#text-center2').css({"color":"#d1d1d1"});
        });
        jQuery('#sendFirst').click(function () {
			var form = document.getElementById('first-form');
            data=new FormData(form);
			jQuery.ajax({
					url: getEmUrl(), 
					type: "POST",             
					data: data,
					contentType: false,  
					dataType: "html",
					cache: false,             
					processData:false,        
				success: function(data)
				{
					jQuery('.tabs').hide();
					jQuery('#tab-3').show();
			jQuery('.progress-bar').css({'width':'60%'});
                        jQuery('#text-center3').css({"color":"#333"});
					jQuery("#ads-preview").html(data);
				}
				});

			return false;
		});
            jQuery('#show_price').click(function () {
                jQuery('#pricelistmodal').modal('show');
                return false;
            });
            update_pricelist(<?php echo $_SESSION['EM_advForm']; ?>);
			update_price4fields();
                        if (jQuery('#week').val() === 0) {
                    jQuery('#week').val(1);
                }
                jQuery('#site').show();
                jQuery('#paper').hide();
                prepare_selects();
            jQuery('#start_date, #year, #week').change(function () {
            update_price();
            });
		
		
		jQuery('#toStep4').click(function () {
						if (confirm('Really pay?')) {
						data="action=EM_action&createInvoice=1&advId=" + jQuery('#advId').val();
						jQuery.ajax({
								url: getEmUrl(), 
								type: "POST",             
								data: data,
		//						contentType: false,  
		//						dataType: "html",
								cache: false,             
								processData:false,        
							success: function(data)
							{	
								jQuery('.tabs').hide();
								jQuery('#tab-4').show();
			jQuery('.progress-bar').css({'width':'85%'});
                        jQuery('#text-center4').css({"color":"#333"});
								jQuery("#tab-4").html(data);
							}
							});
						}
						return false;
					});
					
    });
</script>
<?php endif; ?>