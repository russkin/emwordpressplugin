<?php 
    $uri = explode('/', $_SERVER['REQUEST_URI']);
    $cat = $uri[sizeof($uri)-2];
?>
<div class="cats-widget">
<?php echo $title; ?>

<ul class="nav navmenu-nav nav-pills nav-stacked">
    <li><a href="/<?php echo $page; ?>/"><span class="glyphicon glyphicon-bullhorn"></span><span> Все объявления</span></a></li>
    <li><a href="/podat-obyavlenie/" class="adclassy"><span class="glyphicon glyphicon-bullhorn"></span><span> Подать объявление</span></a></li>
<?php foreach ($categories as $key => $val): ?>
    <li class="<?php echo translit($val); ?><?php if ($cat == $key):?> active<?php endif; ?>"><a href="/<?php echo $page; ?>/<?php echo $key; ?>"> <span><?php echo $val; ?></span></a></li>
<?php endforeach; ?>
</ul>
</div>