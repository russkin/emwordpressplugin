<?php

/*
Plugin Name:  Ethnic Media ads exchange system client
Plugin URI: http://dev.ethnicmedia.us/ru/ethnicmedia_wordpress_plugin.html
Description: Клиент для подключения к системе EthnicMedia ad exchange system
Version: 1.0
Author: Ethnic Media
Author URI: http://ethnicmedia.us
Domain Path: /languages/
Text Domain: em-plugin
*/
/*  Copyright 2015  Ethnic Media  (email: andrew.russkin@ethnicmedia.us)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
// Добавляем пункт в меню
define( 'EM_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'EM_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );


function myplugin_init() {
    load_plugin_textdomain( 'em-plugin', false, 'emadsystem/languages' );
}

add_action( 'init', 'myplugin_init' );

function registerAssetsScripts(){
    wp_enqueue_script( 'custom-script', plugins_url( 'assets/js/bootstrap.min.js', __FILE__ ) );
    wp_enqueue_script('custom-script');
    wp_enqueue_script( 'custom-script1', plugins_url( 'assets/js/jasny-bootstrap.min.js', __FILE__ ),[],'123',false );
    wp_enqueue_script( 'custom-script2', plugins_url( 'assets/js/main.js', __FILE__ ),[],'123',false );
    wp_enqueue_style( 'fontsgoogleapiscom', 'http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic&subset=latin,cyrillic-ext,cyrillic,latin-ext', array(), '20120208', 'all' );
    wp_enqueue_style( 'bootstrap-style', plugins_url( 'assets/css/bootstrap.min.css', __FILE__ ), array(), '20120208', 'all' );
    wp_enqueue_style( 'jasny-bootstrap-style', plugins_url( 'assets/css/jasny-bootstrap.min.css', __FILE__ ), array(), '20120208', 'all' );
    wp_enqueue_style( 'icomoon-style', plugins_url( 'assets/css/icomoon.css', __FILE__ ), array(), '20120208', 'all' );
    wp_enqueue_style( 'main-style', plugins_url( 'assets/css/main.css', __FILE__ ), array(), '20120208', 'all' );
}

add_action( 'wp_print_scripts', 'registerAssetsScripts' );

/**
 * Типа рендерим вьюху
 * @param string $file
 * @param array $param параметры
 */
function render($file,$param){
    if(is_array($param)){
        extract($param,EXTR_PREFIX_SAME,'data');
    }
    if (file_exists(EM_PLUGIN_DIR.'/views/'.$file.'.php')) {
        include EM_PLUGIN_DIR.'/views/'.$file.'.php';
    }else{
        throw new Exception('view file '.EM_PLUGIN_DIR.'/views/'.$file.'.php not found',500);
    }
}
if ( is_admin() ){
	add_action('admin_menu', 'em_add_pages');
	add_action( 'admin_init', 'register_mysettings' );
}
session_start();
//var_dump($_SESSION);
//die();
/**
 * AJAX Callback
 */
function EM_action_callback() {
	$request1= getClient();
   /// AJAX
   /// Обновить прайслист
   if(isset($_POST['updatePricelist']) and isset($_POST['advertise_id'])) {
	   echo $request1->getPricelist($_POST['advertise_id']);
	   wp_die();
   }

   /// Обновить цену
   if(isset($_POST['updatePrice']) and isset($_SESSION['EM_advFormValue'])) {
       $arr=$_SESSION['EM_advFormValue']+ ['content'=>  renderAdvertice()];
	   echo $request1->updatePrice($arr);
	   wp_die();
   }

    /// Шаги мастера подачи объявления
   if(isset($_POST['EM_Step']) and (int)$_POST['EM_Step'] !=5) {
	   renderMaster($_POST['EM_Step']);
	   wp_die();
   }

   /// обновить даты
   if(isset($_POST['getDate']) and isset($_POST['Advertise_id'])) {
	   echo $request1->updateDate($_POST);
	   wp_die();
   }

   /// послать обьявление
   if(isset($_POST['sendForm']) ) {
	   $data = $request1->createAdvertice($_POST);
	   echo (isset($data['preview']))?$data['preview']:'';
	   wp_die();
   }
   /// создать инвойс
   if(isset($_POST['createInvoice']) ) {
	   $data = $request1->createInvoice($_POST['advId']);
	   if(isset($data['status']) and $data['status'] =='paid') {
		   ?>
				<div class="EM_success">Paid!</div>
			<?PHP
	   }elseif(isset($data['status']) and $data['status'] =='success' and isset($data['invoiceId'])){
		   echo $request1->getPaidForm($data['invoiceId']);
		   echo ''
				   . '<script type="text/javascript">'
				   . 'jQuery("#AutorizeForm_AMOUNT").val('.$data['price'].');'
				   . 'jQuery("#invoiceId").val('.$data['invoiceId'].');'
				   . '</script>';
	   }else{
		   var_dump($data);
		   echo "ERROR creating invoice...";
	   }
	   wp_die();
   }

   /// создать инвойс и Оплатить
   if(isset($_POST['payOrder'])) {
       if (isset($_SESSION['EM_invoice'])) { //вторая и далее попытка оплатить
           $data=$_SESSION['EM_invoice'];
       }else{ //первая попытка оплатить
           $data = $request1->createInvoice($_POST['advId']);
           $_SESSION['EM_invoice']=$data;
       }

	   if(isset($data['status']) and $data['status'] =='paid') {
          echo '<div class="EM_success">Paid!</div>';
          wp_die();
       }elseif(isset($data['status']) and $data['status'] =='success' and isset($data['invoiceId'])){
                $_POST['invoiceId'] = $data['invoiceId'];
                $dataPay=$request1->payInvoice($_POST);
                $amt=$_POST['AutorizeForm']['AMOUNT'];
                if(isset($dataPay['status'])) {
                    if($dataPay['status'] =='paid') {
                        render('successPay', ['text'=> __('Invoice', 'em-plugin').'  #'.$dataPay['invoiceId'].' '.__('successfully paid', 'em-plugin')]);
                    }
                    if($dataPay['status'] =='recharge') {
                        render('successPay', ['text'=>__('Not enough funds to pay the invoice, please check your balance in your', 'em-plugin').'  <a href="http://core.ethnicmedia.us/money/payments" target="_BLANK">'.__('personal account', 'em-plugin').'</a>']);

                    }
                    unset($_SESSION['EM_invoice']);
                    unset($_SESSION['EM_advFormValue']);
                    wp_die();
                }else{
                    if(is_array($dataPay)) {
                         foreach ($dataPay as $key => $value) {
                             echo "<p><strong>$key</strong> $value</p>";
                         }
                    }else{
                        $test=(array)json_decode($dataPay);
                        foreach ($test as $key=>$value) {
                            echo "<p><strong>$key</strong> $value[0]</p>";
                        }
                    }
                    EM_showAdFormAjax([
                        'id'        => $_SESSION['EM_advFormValue']['AdvertiseUser']['advertise_id'],
                        'adId'      => (isset($_POST['advId']))?$_POST['advId']:NULL,
                        'price'     => (isset($_POST['AutorizeForm']['AMOUNT']))?$_POST['AutorizeForm']['AMOUNT']:NULL,
                        'invoiceId' => (isset($_POST['invoiceId']) and $_POST['invoiceId'])?$_POST['invoiceId']:null,
                        ],4);
                }

           }
	    wp_die();
   }

   /// авторизация
   if(isset($_POST['auth']) and $_POST['auth']== 'login' ) {
        $_SESSION['authErrors']=null;
	   $data= $request1->login($_POST['login'], $_POST['password']);
	   if(is_array($data)) {
		   foreach ($data as $value) {
			   $_SESSION['authErrors'].= "<div class='red'>$value</div>";
		   };
	   }else{
           $_SESSION['authErrors'] = null;
		   $request1->setUserId($data);
	   }
       EM_showAdForm(['id'=> $_SESSION['EM_advForm']]);
	   wp_die();
   }

   // регистрация
   if(isset($_POST['auth']) and $_POST['auth']== 'register' ) {
       $_SESSION['authErrors']=null;
	   $data= $request1->register($_POST['login'], $_POST['password'],$_POST['Confirmpassword']);
	   if(is_array($data)) {
		   foreach ($data as $value) {
			   $_SESSION['authErrors'].= "<div class='red'>$value</div>";
		   };
	   }else{
           $_SESSION['authErrors'] = null;
		   $request1->setUserId($data);
	   }
	    EM_showAdForm(['id'=> $_SESSION['EM_advForm']]);
	   wp_die();
   }
}

if( defined('DOING_AJAX') && DOING_AJAX ){
	add_action('wp_ajax_EM_action', 'EM_action_callback');
	add_action('wp_ajax_nopriv_EM_action', 'EM_action_callback');
}

/**
 * Регистрация настроек
 */
function register_mysettings() { // whitelist options
  register_setting( 'myoption-group', 'EM_APIKEY' );
  register_setting( 'myoption-group', 'EM_adsLimit' );
  register_setting( 'myoption-group', 'EM_showMenu' );
  register_setting( 'myoption-group', 'EM_debug' );
  register_setting( 'myoption-group', 'EM_adsCategoryUrl' );
  register_setting( 'myoption-group', 'EM_adUrl' );
  register_setting( 'myoption-group', 'EM_searchUrl' );
}

/**
 * Отрисовать объявление
 */
function renderAdvertice(){

    $template = $_SESSION['EM_advFormData']['template'];
    foreach ($_SESSION['EM_advFormData']['fields'] as $field) {
        if ($field['type'] == 'file') {
            if (isset($_SESSION['EM_advFormValue']['FILES'][$field['id']]['name'])) {
                $template=  str_replace('%'.$field['name'].'%', '', $template);
//                $template=  str_replace('%'.$field['name'].'%', '<img style="width: 300px;" alt="'.$_SESSION['EM_advFormValue']['FILES'][$field['id']]['name'].'" src="data:image/png;base64,'.$_SESSION['EM_advFormValue']['FILES'][$field['id']]['content'].'">', $template);
            }else{
                $template=  str_replace('%'.$field['name'].'%', '', $template);
            }
        }else{
            if (isset($_SESSION['EM_advFormValue']['field'][$field['id']])) {
                $template=  str_replace('%'.$field['name'].'%', $_SESSION['EM_advFormValue']['field'][$field['id']], $template);
            }else{
                $template=  str_replace('%'.$field['name'].'%', '', $template);
            }

        }
    }
    return $template;
}

/**
 * Регистрация странички в меню настроек
 */
function em_add_pages() {
    // Add a new submenu under Options:
    add_options_page('Ethnic Media ads exchange system client', 'Ads exchange', 8, 'em_setting', 'mt_options_page');
}

// mt_options_page() displays the page content for the Test Options submenu
function mt_options_page() {
    $client = getClient();
    $client->clearCash();
	?>
	<div class="wrap">
		<h2>Ethnic Media ads exchange system client setting</h2>
			<form method="post" action="options.php">
				<?php wp_nonce_field('update-options'); ?>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">APIKEY</th>
							<td>
								<input placeholder="put your API key here" style="width: 50%;" type="text" name="EM_APIKEY" value="<?= get_option('EM_APIKEY'); ?>" />
							</td>
					</tr>
					<tr valign="top">
                        <th scope="row">Ads category Url</th>
							<td>
								<input style="width: 50%;" type="text" name="EM_adsCategoryUrl" value="<?= get_option('EM_adsCategoryUrl'); ?>" />
							</td>
					</tr>
					<tr valign="top">
                        <th scope="row">Ad Url</th>
							<td>
								<input style="width: 50%;" type="text" name="EM_adUrl" value="<?= get_option('EM_adUrl'); ?>" />
							</td>
					</tr>
					<tr valign="top">
                        <th scope="row">Search Url</th>
							<td>
								<input style="width: 50%;" type="text" name="EM_searchUrl" value="<?= get_option('EM_searchUrl'); ?>" />
							</td>
					</tr>
					<tr valign="top">
						<th scope="row">Default Ad list limit</th>
							<td>
								<input style="width: 50px;" min="0" step="1" type="number" name="EM_adsLimit" value="<?= get_option('EM_adsLimit'); ?>" />
							</td>
					</tr>
<!--					<tr valign="top">
						<th scope="row">Show EthnicMedia menu</th>
							<td>
								<input  type="hidden" name="EM_showMenu" value="0" />
								<input type="checkbox" name="EM_showMenu" <?php if (get_option('EM_showMenu')): ?> checked="" <?php endif ?> value="1" />
							</td>
					</tr>-->
					<tr valign="top">
						<th scope="row">Debug mode</th>
							<td>
								<input  type="hidden" name="EM_debug" value="0" />
								<input type="checkbox" name="EM_debug" <?php if (get_option('EM_debug')): ?> checked="" <?php endif ?> value="1" />
							</td>
					</tr>

				</table>
				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="EM_APIKEY,EM_adsLimit,EM_debug,EM_adsCategoryUrl,EM_adUrl,EM_searchUrl" />
				<p class="submit">
					<input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
				</p>
			</form>
	</div>
<?php
}


function getClient(){
	include_once __DIR__.'/lib/EMclient.php';
	$request= new EMclient(get_option('EM_APIKEY'), 'en', get_option('EM_debug'), get_option('EM_adsLimit'));
	return $request;
}

add_shortcode( 'EM_showAdForm', 'EM_showAdForm' );

/**
 * Рендерим и обрабатываем обьявление
 * @param int $step
 */
function renderMaster($step=1){
    if (!isset($_SESSION['EM_advFormValue'])) {
        $_SESSION['EM_advFormValue']=[];
    }
		if(isset($_FILES)) {
			foreach ($_FILES as $key=>$file) {
				if(file_exists($file['tmp_name'])) {
                    $name=  explode('_', $key);
					$_SESSION['EM_advFormValue']['FILES'][$name[1]] = [
                        'name'    => $file['name'],
                        'content' => base64_encode(file_get_contents($file['tmp_name'])),
                    ];
				}
			}
		}
    $_SESSION['EM_advFormValue'] = my_merge_recursive($_SESSION['EM_advFormValue'],$_POST);
    if (!isset($_SESSION['EM_advFormValue']['AdvertiseUser']['advertise_id'])) {
        return;
    }
    switch ($step) {
        case 4:
            EM_getPayForm();
            break;

        default:
            EM_showAdFormAjax(['id'=>$_SESSION['EM_advFormValue']['AdvertiseUser']['advertise_id']],$step);
            break;
    }

}

/**
 * Сливаем 2 массива рекурсивно
 * @param type $start
 * @param type $new
 */
function my_merge_recursive($start,$new){
    $return = [];
    if (!$start) {
        return $new;
    }
    foreach ($new as $key => $newValue) {
        if (array_key_exists($key, $start)) {
            if (is_array($newValue)) {
                $start[$key]=  my_merge_recursive($start[$key],$newValue);
            }else{
                $start[$key]=$newValue;
            }
        }else{
            $start[$key] = $newValue;
        }
    }
    return $start;
}


function EM_saveAd(){
    $request= getClient();
    return $request->createAdvertice($_SESSION['EM_advFormValue']);

}


function EM_getPayForm(){
    if ($result=EM_saveAd()) {
        if (isset($result['price']) and $result['price'] ==0 ) {
             render('successPay', ['text'=> __('Thank you, your ad will be published in the soon', 'em-plugin')]);
        }
        EM_showAdFormAjax([
            'id'    => $_SESSION['EM_advFormValue']['AdvertiseUser']['advertise_id'],
            'adId'  => (isset($result['advertiseId']))?$result['advertiseId']:NULL,
            'price' => (isset($result['price']))?$result['price']:NULL,
            ],4);
    }else{

        var_dump($result);
        EM_showAdFormAjax(['id'=>$_SESSION['EM_advFormValue']['AdvertiseUser']['advertise_id']],4);
    }
}

/**
 * Вывести форму подачи обьявления
 * @param array $param
 */
function EM_showAdForm($param,$step=1){
    $id=(isset($param['id']))?$param['id']:null;
    $_SESSION['EM_advForm']=$id;
//    var_dump($_SESSION['EM_advForm']);
    $request= getClient();
    if ($authForm =$request->getAuthForm()) {
        echo (isset($_SESSION['authErrors']))?$_SESSION['authErrors']:'';
        echo $authForm;
        $_SESSION['authErrors']=null;
        return;
    }
    $_SESSION['EM_advFormData']=$request->getAdverticeForm($id, 'json');
    $_SESSION['EM_advFormValue'] = [];
    render('classy_add',[
        'adContent'    => $_SESSION['EM_advFormData'],
        'adCategoryes' => $request->getAdCategoryList(),
            ]);
}

function EM_showAdFormAjax($param,$step=1){
    $id=(isset($param['id']))?$param['id']:null;
    $_SESSION['EM_advForm']=$id;
    $request= getClient();
    $_SESSION['EM_advFormData']=$request->getAdverticeForm($id, 'json');
    render('classy_add'.$step,[
        'adContent'    => $_SESSION['EM_advFormData'],
        'adCategoryes' => $request->getAdCategoryList(),
        'params'        => $param,
            ]);
    wp_die();
}

/**
 * вывести дерево категорий
 */
function EM_show_cats() {
    $client = getClient();
	$categories = $client->getAdCategoryList();
    $searchParam=(isset($_GET['searchParam']) and is_array($_GET['searchParam']))?(array)$_GET['searchParam']:[];
	$pageUrl = get_option('EM_adsCategoryUrl');
    render('categoryList', [
        'categories'  => $categories,
        'pageUrl'     => $pageUrl,
        'searchParam' => $searchParam,
    ]);
}
add_shortcode( 'em-cats', 'EM_show_cats' );

/**
 * Список обьявлений
 */
function EM_getAds(){
    $searchParam=(isset($_GET['searchParam']) and is_array($_GET['searchParam']))?(array)$_GET['searchParam']:[];
    $client = getClient();
    $categoryId=(isset($_GET['catUrl']))?(int)$_GET['catUrl']:null;
    $format = 'html';
    $limit=get_option('EM_adsLimit');
    $page = (isset($_GET['pageAd']))?(int)$_GET['pageAd']:1;
    $ads=$client->getAds($categoryId, $format, $limit, $page);
    $pagination = $client->getPaginationArray($categoryId, $limit);
    $categories = $client->getAdCategoryList();
    render('adList', [
        'basePaginationUrl'    => '/'.get_option('EM_adsCategoryUrl').($categoryId)?'?catUrl='.$categoryId.'&pageAd=':'?pageAd=',
        'ads'        => $ads,
        'page'       => $page,
        'pagination' => $pagination,
        'categories' => $categories,
        'searchParam'=> $searchParam,
        ]);
}
add_shortcode( 'em-categoryAds', 'EM_getAds' );


/**
 * Поиск обьявлений
 */
function EM_searchAds(){
    $client = getClient();
    $searchParam=(isset($_GET['searchParam']) and is_array($_GET['searchParam']))?(array)$_GET['searchParam']:[];
    $format = 'html';
    $searchParamstr='';
    foreach ($searchParam as $key => $value) {
        $searchParamstr.='&searchParam['.$key.']='.$value;
    }
    $limit=get_option('EM_adsLimit');
    $page = (isset($_GET['pageAd']))?(int)$_GET['pageAd']:1;
    $ads=$client->searchAds($searchParam, $format, $limit, $page);
    $pagination = $client->getPaginationArray($searchParam, $limit);
    $categories = $client->getAdCategoryList();
    render('adList', [
        'basePaginationUrl'    => '/'.get_option('EM_searchUrl').($searchParamstr)?'?searchAd=1'.$searchParamstr.'&pageAd=':'?searchAd=1&pageAd=',
        'ads'        => $ads,
        'page'       => $page,
        'searchParam'=> $searchParam,
        'pagination' => $pagination,
        'categories' => $categories
        ]);
}
add_shortcode( 'em-searchAds', 'EM_searchAds' );

function EM_getAd(){
    $adId=(isset($_GET['adId']))?(int)$_GET['adId']:null;
    $client = getClient();
    $ad=$client->getAd($adId );
    $categories = $client->getAdCategoryList();
    render('adSingle',['ad'=>$ad,'categories' => $categories]);

}
add_shortcode( 'em-Ad', 'EM_getAd' );