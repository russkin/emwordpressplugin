<?php

/**
 * Shortcodes for Ethnic Media Ads Exchange Plugin
 */

function EM_show_cats() {
	$categories = EM_getAdsCats();
    var_dump($categories);
    return;
	$page_url = get_option('EM_archive_page');
	ob_start();
	?>
	<nav class="nav_bar">
		<div class="container-fluid cats-bussines">
			<div class="navmenu offcanvas-sm">
				<ul class="nav navmenu-nav nav-pills nav-stacked">
					<?php
						// <img src="' . get_template_directory_uri() . '/media/img/ads_cat_icon_' . $id . '.png" class="icon">
						foreach ( $categories as $id => $title ) {
							echo '<li><a href="' . $page_url . '?adcat=' . $id . '"> <span>' . $title . '</span></a></li>';
						}
					?>
				</ul>
			</div>
		</div>
	</nav>
<?php
	$cats = ob_get_contents();
	return $cats;
	ob_end_clean();
}
add_shortcode( 'em-cats', 'EM_show_cats' );

function EM_show_forms() {
	$forms = getAdFomList();
	$html = '';
	foreach ($forms as $key => $title) {
		$html .= EM_showAdForm( $key );
	}
	return $html;
}
add_shortcode( 'em-forms', 'EM_show_forms' );

function EM_pay_forms() {
	$atts = shortcode_atts( array(
		'id' => 0,
	), $atts );
	$forms = getAdFomList();
	$html = '';
	foreach ($forms as $key => $title) {
		$html = EM_showAdForm( $key );
	}
//	die($atts['id']);
render("pay-form",array(
						'form_id' => $atts['id'],
						'em_form' => $html,
						'pay_form' => EM_getPayForm( $atts['id'] ),
						'category' => EM_getAdsCats()
						));
}
add_shortcode( 'em-pay', 'EM_pay_forms' );


function EM_GetAdsShC( $atts ) {
    $uri = explode('/', $_SERVER['REQUEST_URI']);
    if ($_GET['n']){
        $data = EM_getAd( $_GET['n'] );
/*        echo "<pre>";
        print_r($data);
        die();*/
        $categories = EM_getAdsCats();
        foreach ($categories as $key => $val){
            if ($data['data']['category_id'] == $key){
                $category = array('id' => $key, 'name' => $val);
            }
        }
        render("ads-detail", array('ads' => $data['json'], 'category' => $category, 'page' => $uri[1]));
    } else {
            $atts = shortcode_atts( array(
                    'type' => 'json',
                    'limit' => 20,
            ), $atts );

        $cat = $uri[2];
        $ads_list = '';
        $ads = EM_getAds($cat,$atts['type'],$atts['limit']);
        if ( $atts['type'] == 'json' ) {
            $ads_list = $ads;
            if ( empty( $ads ) ) {
                $ads_list = '<p>'._e('Sorry, no posts matched your criteria.').'</p>';
            }
        } else {
            $ads_list = $ads;
        }
        render("ads-form",array('ads' => $ads_list));
    }
}
add_shortcode( 'em-ads', 'EM_GetAdsShC' );

function EM_getSingleAd( $id ) {

	$id = $_GET['n'];

	$data = EM_getAd( $id );
	return $data['html'];
}
add_shortcode( 'em-ad', 'EM_getSingleAd' );