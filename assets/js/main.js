function showHide(advant_search) {
    if (document.getElementById(advant_search)) {
        var obj = document.getElementById(advant_search);
        if (obj.style.display != "block") {
            obj.style.display = "block";
        } else
            obj.style.display = "none";
    } else
        alert("Элемент с id: " + advant_search + " не найден!");
}

function getEmUrl(){
    return "/wp-admin/admin-ajax.php";
}


jQuery(function () {
    jQuery('[data-toggle="tooltip"]').tooltip()
});
