<?php 

/**
 * Shortcodes for Ethnic Media Ads Exchange Plugin
 */

function EM_show_cats() {
	$categories = EM_getAdsCats();
	$page_url = get_option('EM_archive_page');
	ob_start();
	?>
	<nav class="nav_bar">
		<div class="container-fluid cats-bussines">
			<div class="navmenu offcanvas-sm">
				<ul class="nav navmenu-nav nav-pills nav-stacked">
					<li><a href="<?php echo home_url(); ?>" class="back"><span class="icon-arrow-left2"></span> <span>Вернуться на новости</span></a></li>
					<li><a href="<?php echo get_option('EM_form_page'); ?>" class="adclassy"><span class="glyphicon glyphicon-bullhorn"></span><span> Подать объявление</span></a></li>
					<?php 
						// <img src="' . get_template_directory_uri() . '/media/img/ads_cat_icon_' . $id . '.png" class="icon">
						foreach ( $categories as $id => $title ) {
							echo '<li><a href="' . $page_url . '?adcat=' . $id . '"> <span>' . $title . '</span></a></li>';
						}
					?>
				</ul>
			</div>
		</div>
	</nav>
<?php 
	$cats = ob_get_contents();
	return $cats;
	ob_end_clean();
}
add_shortcode( 'em-cats', 'EM_show_cats' );

function EM_show_forms() {
	$forms = getAdFomList();
	$html = '';
	if (isset($_SESSION['userEmId']) and $_SESSION['userEmId']) {
		foreach ($forms as $key => $title) {
			$fields = EM_showAdForm( $key, 'json' );

			if ( isset( $fields['fields'] ) && ! empty( $fields['fields'] ) ) {
				usort( $fields['fields'], function($a, $b) {
					return $a['order'] - $b['order'];
				});
			
				// echo '<pre>';
				// print_r($fields);
				// echo '</pre>';

				$html .= '
				<div class="form_classyadd">
				<form class="form-horizontal" enctype="multipart/form-data" action="/" id="advertise-form-main" method="post">
					<input type="hidden" name="action" value="EM_action">
					<input type="hidden" name="create_banner" value="0" id="create_banner">
					<input name="AdvertiseUser[id]" id="AdvertiseUser_id" type="hidden">
					<input name="AdvertiseUser[advertise_id]" id="AdvertiseUser_advertise_id" type="hidden" value="' . $fields['id'] . '">
				';
				
				$html .= '<div id="step1" class="steppy">';
				$html .= '
					<div class="box col-md-12 ">
						<div class="box-inner">
							<div class="box-content">
								<div id="">
									<div class="pull-left control-group">
										<label for="AdvertiseUser_start_date">Неделя</label> 
										<div class="controls">
											<input type="number" id="start_date" title="" name="AdvertiseUser[start_date]" min="1" max="53" value="38">
											<div style="margin-top: 10px; margin-bottom: 10px; padding-right: 19px; width: 145px; padding-top: 5px; padding-bottom: 5px; text-align: center;" class="well">
												<span id="s_date">2016-08-21</span>
											</div>
										</div>
									</div>
									<div id="shedule_div" rel="paper" class="pull-left control-group">
									</div>
									<div class="pull-left control-group">
										<label for="AdvertiseUser_year">Year</label> 

										<div class="controls">
											<input type="number" id="year" title="" name="AdvertiseUser[year]" min="2015" max="" value="2015">
										</div>
									</div>
									<div id="site" class="pull-left control-group">
										<label for="AdvertiseUser_weeks">Недель размещения</label> 
										<div class="controls">
											<input type="number" id="week" title="" name="AdvertiseUser[weeks]" min="1" max="53" value="1">
											<div style="margin-top: 10px; margin-bottom: 10px; padding-right: 19px; width: 145px; padding-top: 5px; padding-bottom: 5px; text-align: center;" class="well">
												<span id="e_date">2016-09-03</span>
											</div>
										</div>
									</div>
									<div id="paper" style="display:none;">
										<div class="pull-left control-group" rel="paper">
											<label class="control-label" for="AdvertiseUser_issues">Выпуски</label>
											<div class="controls">
												<input min="1" style="width:50px;" name="AdvertiseUser[issues]" id="AdvertiseUser_issues" type="number">
												<p id="AdvertiseUser_issues_em_" style="display:none" class="help-block"></p>
											</div>
										</div>
									</div>
								</div>
								<div id="pricelist" style="" class="pull-left control-group">
									<div class="controls" style="margin-top: 25px;">
										<a class="btn btn-warning" href="#" id="show_price">Показать прайслист</a>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				';

				$categories = EM_getAdsCats();

				$html .= '<select name="AdvertiseUser[category_id]" id="AdvertiseUser_category_id">';
				foreach ($categories as $cat_id => $cat_title) {
					$html .= '<option value="' . $cat_id . '">' . $cat_title . '</option>';
				}
				$html .= '</select>';

				foreach ($fields['fields'] as $field) {
					$html .= format_field( $field );
				}
				
				$html .= '<ul class="pager steps-pager">';
				//$html .= '<li class="previous disabled"><a href="#">Prev</a></li>';
				$html .= '<li class="next"><a data-step="step2" href="#">Next</a></li>';
				$html .= '</ul>';

				$html .= '</div>';//step1

				$html .= '<div id="step2" class="steppy" style="display: none;">';
				foreach ( $fields['servises'] as $s_id => $value ) {
					$html .= format_services( $s_id, $value );
				}

				$html .= '<ul class="pager steps-pager">';
				$html .= '<li class="previous"><a data-step="step1" href="#">Prev</a></li>';
				$html .= '<li class="next"><a data-step="step3" href="#">Next</a></li>';
				$html .= '</ul>';

				$html .= '</div>';//step2

				$html .= '<div id="step3" class="steppy" style="display: none;">';
				// get data from inputs by jquery
				$html .= '<div class="ad-preview">';
				$html .= '<img id="preview-117">';
				$html .= '<div id="preview-111"></div>';
				$html .= '<div id="preview-112"></div>';
				$html .= '<div id="preview-113"></div>';
				$html .= '<div id="preview-category"></div>';
				$html .= '<div id="preview-114"></div>';
				$html .= '<div id="preview-115"></div>';
				$html .= '<div id="preview-116"></div>';
				$html .= '<div id="preview-110"></div>';
				$html .= '</div><!--ad preview-->';

				$html .= '<ul class="pager steps-pager">';
				$html .= '<li class="previous"><a data-step="step2" href="#">Prev</a></li>';
				$html .= '<li class="next"><a data-step="step4" href="#">Next</a></li>';
				$html .= '</ul>';

				$html .= '</div>';//step3


				$html .= '<div id="step4" class="steppy" style="display: none;">';
				$html .= '<div class="progress" style="width:40%; margin:10px auto;">';
				$html .= '<div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">';
				$html .= '</div>';
				$html .= '</div>';

				$html .= '<ul class="pager steps-pager">';
				$html .= '<li class="previous"><a data-step="step3" href="#">Prev</a></li>';

				$html .= '</div>';//step4
		

				$html .= '<div class="amount">';
				$html .= '<h1>TOTAL: <span style="color:red" id="AMT">';
				$html .= '0</span></h1>';
				$html .= '</div>';

				$html .= '<a href="#" class="btn btn-primary" id="sendForm">Send</a>';
				$html .= '<input type="hidden" name="sendForm" value="1">';


				$html .= '</form>';
				$html .= '</div>';
				$html .= '
				<script>
					function readURL(input,prev_id) {
					    if (input.files && input.files[0]) {
					        var reader = new FileReader();

					        reader.onload = function (e) {
					            jQuery(prev_id).attr(\'src\', e.target.result);
					        }

					        reader.readAsDataURL(input.files[0]);
					    }
					}

					jQuery(function () {

						jQuery(\'.steps-pager > li:not(.disabled) > a\').on(\'click\', function(event){
							event.preventDefault();
							var target = jQuery(this).data(\'step\');
							jQuery(this).closest(\'.steppy\').hide();
							jQuery(\'.steps > .col-md-3 .text-center\').addClass(\'disabled\');
							jQuery(\'.steps .\'+target).removeClass(\'disabled\');
							jQuery(\'#\'+target).show();
						});

						jQuery(\'.steps-pager .next:not(.disabled) a\').on(\'click\', function(event){
							event.preventDefault();
							var progressWidth = jQuery(\'.steps .steps-progress .progress-bar\').width();
								progressParent = jQuery(\'.steps .steps-progress .progress-bar\').offsetParent().width();
								progress = 100*progressWidth/progressParent;
								newWidth = progress + 25;
								if ( newWidth > 100 ) {
									newWidth = 100;
								}

							jQuery(\'.steps .steps-progress .progress-bar\').css(\'width\',newWidth+\'%\')
						});

						jQuery(\'.steps-pager .previous:not(.disabled) a\').on(\'click\', function(event){
							event.preventDefault();
							var progressWidth = jQuery(\'.steps .steps-progress .progress-bar\').width();
								progressParent = jQuery(\'.steps .steps-progress .progress-bar\').offsetParent().width();
								progress = 100*progressWidth/progressParent;
								newWidth = progress - 25;
								if ( newWidth < 0 ) {
									newWidth = 0;
								}

							jQuery(\'.steps .steps-progress .progress-bar\').css(\'width\',newWidth+\'%\')
						});

						jQuery(\'.fields_adv\').on(\'change\',function(event) {
							var prev_id = jQuery(this).data(\'preview\');
							if ( jQuery(this).attr(\'type\') == \'file\') {
								readURL(this, prev_id);
							} else {
								jQuery(prev_id).html(jQuery(this).val());
							}
						});
						limit = parseInt(jQuery(\'#char_limit\').html());
						count = 0;
						jQuery(\'.fields_adv\').each(
								function () {
									if (jQuery(this).attr(\'type\') !== \'file\') {
										count = count + jQuery(this).val().length;
									}

								});
						if (count > limit) {

							jQuery(\'#char_left\').html(0);
						} else {
							jQuery(\'#char_left\').html(limit - count);
						}
						jQuery(\'.fields_adv\').keyup(function () {
							limit = parseInt(jQuery(\'#char_limit\').html());
							count = 0;
							jQuery(\'.fields_adv\').each(
									function () {
										count = count + jQuery(this).val().length;
									});
							if (count > limit) {
								jQuery(this).val(jQuery(this).val().substr(0, jQuery(this).val().length - 1));
								jQuery(\'#char_left\').html(0);
							} else {
								jQuery(\'#char_left\').html(limit - count);
							}
						});
					});
					function update_price4fields() {
						var vars = \'advertise_id=\' + jQuery(\'#AdvertiseUser_advertise_id\').val() + \'&weeks=\' + jQuery(\'#week\').val() + \'&issues=\' + jQuery(\'#AdvertiseUser_issues\').val();
						vars = vars + \'&year=\' + jQuery(\'#year\').val()+\'&updatePrice=1&action=EM_action\';
						var content = \'\';
						jQuery(\'.fields_adv\').each(
								function () {
									if (jQuery(this).val()) {
										content += \' \' + jQuery(this).val();
									}
								});
						vars += \'&content=\' + content;
						jQuery(\'input:checkbox:checked\').each(
								function () {
									if (jQuery(this).attr(\'rel\') == \'serv\') {

										vars += \'&ids[\' + jQuery(this).attr(\'id\') + \']=\' + jQuery(this).val();
									}
								});

						jQuery(\'.serv_val\').each(
								function () {
									vars += \'&value[\' + jQuery(this).attr(\'id\') + \']=\' + jQuery(this).val();
								});
						jQuery.ajax({
							url: getEmUrl(),
							data: vars,
							// dataType: "json",
							type: "POST",
							timeout: 10000,
							success: function (data) {
								jQuery(\'#AMT\').html(data);
							},
							error: function (xhr, ajaxOptions, thrownError) {
							}
						});

					}


					jQuery(function () {
						var advertise_id = jQuery(\'#AdvertiseUser_advertise_id\').val();
									jQuery(\'.servise, .serv_val, #week, .fields_adv, #AdvertiseUser_issues\').change(function () {
							update_price4fields();
						});

						jQuery(\'.servise\').change(function () {
							if (jQuery(this).attr(\'title\') == \'make banner\' && jQuery(this).prop("checked")) {
								jQuery(\'#sendMail\').modal(\'show\');
							}
							if (jQuery(this).attr(\'title\') == \'make banner\' && !jQuery(this).prop("checked")) {
								jQuery(\'#email\').val(\'\');
							}
						});
					});
					//  обновляем список сайтов
						function update_site(obj) {
						}

						function prepare_selects() {
						}
					//  обновляем список языков
						function update_lang() {
						}

						function updateSiteTypes() {
						}
					//  обновляем список категорий сайта
						function update_ads_cats() {

						}
					//  обновляем список обьявлений
						function update_adv() {
						}
					//  Собираем поля и сервисы до кучи
						function make_fields_nServises() {
						}
						function update_price() {
							Advertise_id = jQuery(\'#AdvertiseUser_advertise_id\').val();
							start_date = jQuery(\'#start_date\').val();
							weeks = jQuery(\'#week\').val();
							year = jQuery(\'#year\').val();
							jQuery.ajax({
								type: \'POST\',
								url: getEmUrl(),
								dataType: "json",
								data: {Advertise_id: Advertise_id, start_date: start_date, weeks: weeks, year: year,getDate:1,action: "EM_action"},
								success: function (data) {
									jQuery(\'#s_date\').html(data.start_date);
									jQuery(\'#e_date\').html(data.end_date);
								}
							});
						}

						function update_pricelist(adv) {
							if (!adv) {
								advertise_id = jQuery(\'#AdvertiseUser_advertise_id\').val();
							} else {
								advertise_id = adv;
							}

							jQuery.post(getEmUrl(), {advertise_id: advertise_id,updatePricelist:1,action: "EM_action"})
									.done(function (data) {
										if (data) {
											jQuery(\'#pricelist\').show();
											jQuery(\'#price_modal_body\').html(data);
										} else {
											jQuery(\'#pricelist\').hide();
										}
									})
									.fail(function () {
										alert("error!");
									})
						}

						jQuery(function () {
							jQuery(\'#sendForm\').click(function () {
								var form = document.getElementById(\'advertise-form-main\');
								data=new FormData(form);
								if(confirm(\'Really send?\')) {
									jQuery.ajax({
										url: getEmUrl(),
										type: "POST",
										data: data,
										contentType: false,
										dataType: "html",
										cache: false,
										processData:false,

										success: function(data)
										{
											jQuery("#adv_form").html(data);
										}
									});
								}

								return false;
							});
						jQuery(\'#show_price\').click(function () {
							jQuery(\'#pricelistmodal\').modal(\'show\');
							return false;
						});
						update_pricelist(' . $fields['id'] . ');
						update_price4fields();
						if (jQuery(\'#week\').val() === 0) {
							jQuery(\'#week\').val(1);
						}
						jQuery(\'#site\').show();
						jQuery(\'#paper\').hide();
						prepare_selects();
						jQuery(\'#start_date, #year, #week\').change(function () {
							update_price();
						});
					});
				</script>
			';
			} else {



			}//endif empty($fields)
		}
	} else {
		foreach ($forms as $key => $title) {
			$html .= EM_showAdForm( $key );
		}
	}
	echo $html;
}
add_shortcode( 'em-forms', 'EM_show_forms' );

function format_field( $in_field ) {
	// echo '<pre>';
	// print_r($in_field);
	// echo '</pre>';
	if ( $in_field['requre'] ) {
		$required = 'required="required" ';
	} else {
		$required = '';
	}
	$out_field  = '<div class="form-group">';
	$out_field .= '<label for="' . $in_field['id'] . '" class="col-sm-2 control-label">' . $in_field['description'] . '</label>';
	$out_field .= '<div class="col-sm-10">';
	switch ( $in_field['type'] ) {
		case 'textarea':
			$out_field .= '<textarea ' . $required . ' class="fields_adv value form-control" name="field[' . $in_field['id'] . ']" id="' . $in_field['id'] . '" data-preview="#preview-' . $in_field['id'] . '" placeholder="' . $in_field['description'] . '"></textarea>';
			$out_field .= '<div class="classytext_counters">
							<div class="counter">Лимит символов: <span id="char_limit">500</span></div>
							<div class="counter">Символов осталось: <span id="char_left">500</span></div>
						  </div>';
			$out_field .= '<p>Пункты отмеченные <span class="required">*</span>, обязательны для заполнения.</p>';
			break;
		case 'file':
			$out_field .= '<input ' . $required . ' name="image_' . $in_field['id'] . '" data-preview="#preview-' . $in_field['id'] . '" type="' . $in_field['type'] . '" class="fields_adv value form-control" id="' . $in_field['id'] . '" placeholder="' . $in_field['description'] . '">';
			break;
		default:
			$out_field .= '<input ' . $required . ' name="field[' . $in_field['id'] . ']" data-preview="#preview-' . $in_field['id'] . '" type="' . $in_field['type'] . '" class="fields_adv value form-control" id="' . $in_field['id'] . '" placeholder="' . $in_field['description'] . '">';
			break;
	}
	$out_field .= '</div>';
	$out_field .= '</div>';
	return $out_field;
}

function format_services( $s_id, $in_service ) {
	$s_id = '9'.$s_id;

	$out_service  = '<div class="form-group">';
	$out_service .= '<label for="s_' . $s_id . '">' . $in_service['name'] . '</label>';
	switch ($in_service['type']) {
		case 'checkbox':
			$out_service .= '<input rel="serv" class="servise" title="On top for" type="checkbox" name="servises[' . $s_id . ']" id="s_' . $s_id . '">';
			break;

		case 'number':
			$out_service .= '<input rel="serv" class="servise" title="On top for" type="checkbox" name="servises[' . $s_id . ']" id="s_' . $s_id . '">';
			$out_service .= '<input name="value[' . $s_id . ']" type="' . $in_service['type'] . '" class="serv_val value form-control" min="0" id="v_' . $s_id . '" placeholder="' . $in_service['name'] . '">';
			break;
	}
	$out_service .= '</div>';
	return $out_service;

}

function format_payform( $field ) {

	$id = str_replace(array('[',']'), array('_',''), $field['name']);
	$error_id = str_replace(array('[',']'), array('_','_em_'), $field['name']);

	$pattern = '';
	switch ($field['name']) {
		case 'AutorizeForm[CC]':
			$pattern = 'pattern="[0-9]{4} [0-9]{4} [0-9]{4} [0-9]{4}" ';
			break;

		case 'AutorizeForm[EXPMNTH]':
			$pattern = 'pattern="[0-9]{2}" ';
			break;

		case 'AutorizeForm[EXPYR]':
			$pattern = 'pattern="[0-9]{2}" ';
			break;

		case 'AutorizeForm[CSC]':
			$pattern = 'pattern="[0-9]{4}" ';
			break;

		case 'AutorizeForm[CSC]':
			$pattern = 'pattern="[0-9]{4}" ';
			break;
	}


	$html  = '<div class="form-group">';
	$html .= '<label for="' . $id . '" class="col-sm-3 control-label">' . $field['label'] . '</label>';
	$html .= '<div class="col-sm-9">';
	$html .= '<input ' . $pattern . 'name="' . $field['name'] . '" type="' . $field['type'] . '" class="form-control" value="' . $field['value'] . '" id="' . $id . '" placeholder="' . $field['label'] . '">';
	$html .= '<div class="errorMessage" id="' . $error_id . '" style="display:none"></div>';
	$html .= '</div>';
	$html .= '</div>';
	return $html;
}

function EM_GetAdsShC( $atts ) {
	
	$atts = shortcode_atts( array(
		'cat' => null,
		'type' => 'json',
		'limit' => 20,
	), $atts );

	$ads = EM_getAds($atts['cat'],$atts['type'],$atts['limit']);

	if ( $atts['type'] == 'json' ) {

		//$ads = json_decode( $ads );
		$html = '';
		if ( ! empty( $ads ) ) {
			foreach ($ads as $key => $ad) {
				//238
				$html .=  '<div class="classy-item">';
				$html .= '<a href="' . get_option('EM_single_page') . '?ad=' . $ad['id'] . '"><h4>' . $ad['FIELDS'][4]['value'] . '</h4></a>';
				$html .= '<data>' . date('m.d.Y', $ad['start_date'] ) . '</data>';
				$html .= '<div class="view"><span class="icon-eye"></span> 123</div>';
				$html .= '<p>' . mb_substr( $ad['FIELDS'][6]['value'], 0, 238, 'UTF-8' ) . '...</p>';
				$html .= '</div>';
			}
		} else {
			echo '<p>'._e('Sorry, no posts matched your criteria.').'</p>';
		}
		
		return $html;

	} else {
		return $ads;
	}

}
add_shortcode( 'em-ads', 'EM_GetAdsShC' );

function EM_getSingleAd( $atts ) {

	$atts = shortcode_atts( array(
		'id' => 0,
	), $atts );

	$data = EM_getAd( $atts['id'] );
	return $data['html'];
}
add_shortcode( 'em-ad', 'EM_getSingleAd' );