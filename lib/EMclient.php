<?php

/**
 * Клиент для REST servis
 *
 * @author Andrew Russkin <andrew.russkin@gmail.com>
 */
class EMclient {

	/*  API KEY для сайта */
	public $APIKEY='';

	/** @var string  */
	protected $serverUrl='http://api.ethnicmedia.us';

	/** @var string  */
	public $checkLoginUrl = "http://core.ethnicmedia.loc/logo.png";

	/** @var bool  */
	public $debug=true;

	/** @var integer  */
	public $adsLimit=20;


	/** @var FirePHP  */
	protected $FB;

	/** @var Request  */
	protected $request;

	/**
	 * разлогиневание в системе
	 * @var string
	 */
	protected $logoutUri='?mode=logout';

	/** @var string  */
	protected $lang;

	/** @var string  */
	protected $userID;

	function __construct($apiKey,$lang='ru',$debug=true,$adsLimit=20) {
		$this->APIKEY=$apiKey;
		$this->debug=$debug;
		$this->adsLimit=$adsLimit;
        if ($_SERVER['HTTP_HOST'] == 'client.loc') {
            $this->serverUrl = 'http://api.ethnicmedia.loc';
        }
		if ($this->debug) {
			include_once 'FirePHP/FirePHP.class.php';
			$this->FB= FirePHP::getInstance(true);
		}
		$haystack=array('en','ru');
		if (isset($_COOKIE['EM_lang']) and in_array($_COOKIE['EM_lang'], $haystack)) {
			$lang=$_COOKIE['EM_lang'];
		}
		$this->lang=$lang;
		$this->userID=(isset($_SESSION['userEmId']) and $_SESSION['userEmId'])?$_SESSION['userEmId']:null;
		include_once 'Request.php';
		$this->request=new Request($this->debug, $this->serverUrl);

	}

    /**
     * Очистить кэш
     * @return bool
     */
    public function clearCash(){
        unset($_SESSION['userEmId']);
        unset($_SESSION['EM_advFormData']);
        unset($_SESSION['EM_advFormValue']);
        return $this->request->clearCash();
    }

    /**
	 * Получить массив для построения пагинации
	 * @param int||array $searchParam
	 * @param int $limit
	 * @return array
	 */
	public function getPaginationArray($searchParam,$limit=20){
		$return=array();
        if (is_int($searchParam) or $searchParam === null) {
			$data=$this->request->getRequest(
				'GET', '/getadsmaxpage', 'API_KEY='.$this->APIKEY.'&CATEGORYID='.$searchParam.'&LIMIT='.$limit,
				$data=array());
        }
        if (is_array($searchParam)) {
			$data=$this->request->getRequest(
				'GET', '/getsearchadsmaxpage', 'API_KEY='.$this->APIKEY.'&SEARCHPARAM='.json_encode($searchParam).'&LIMIT='.$limit,
				$data=array());
        }
		if($data) {
			for ($i = 1; $i <= $data; $i++) {
				$return[$i]=$i;
			}
		}
		return $return;
	}

	/**
	 *  Аутентификация пользователя
	 * @return string
	 */
	public function autentificate($userData){
		if ($userData['userEmId']) {
			$this->userID=$userData['userEmId'];
			return true;
		}
		return $this->request->getRequest('GET', '/login', 'API_KEY='.$this->APIKEY.'&USERNAME='.$userData['login'].'&PASSWORD='.$userData['pass'], array(), 'login');
	}

	/**
	 * Получить полностью всю болванку
	 */
	public function getAllLayout(){
		return $this->request->getRequest(
				'GET',
				'/getalllayout',
				$this->getUri().'&LOGOUTURI='.$this->logoutUri,
				$data=array(),
				'main_layout_'.$this->userID.'_'.$this->lang);
	}

	/**
	 * Получить основную разметку плашки
	 * @return string
	 */
	public function getLayout(){
		return $this->request->getRequest('GET', '/getlayout', $this->getUri(), $data=array(), 'main_layout');
	}

	/**
	 * Получить категории объявлений
	 * @param int $formId
	 * @param string $type 'html' || 'json'
	 * @param array $options (showDate,showServises,showPricelist,includeJs)
	 */
	public function getAdCategoryList(){
		return $this->request->getRequest('GET', '/getadcategorylist', $this->getUri(), $data=array(), 'AdCategoryList'.$this->lang);
	}

    /**
     * Проверяем авторизацию и если клиент не авторизован выводим форму логина
     * @return string|| bool
     */
    public function getAuthForm(){
    	if(!$this->userID) {
			return $this->request->getRequest('GET', '/getauthform', $this->getUri().'&SHOWADVFORM=1', $data=array(), 'auth_form_'.$this->lang);
		}
        return false;

    }


    /**
	 * Получить форму для подачи объявления
	 * @param int $formId
	 * @param string $type 'html' || 'json'
	 * @param array $options (showDate,showServises,showPricelist,includeJs)
	 */
	public function getAdverticeForm($formId, $type,$options=array('showDate'=>true,'showServises'=>true,'showPricelist'=>true,'includeJs'=>true)){
		if(!in_array($type, array('html','json'))) {
			throw new Exception('Wrong return type format', 500);
		}
		return $this->request->getRequest('GET', '/adform', $this->getUri().'&ADFORM='.(int)$formId.'&TYPE='.$type.'&DATA='.json_encode($options), $data=array(), 'adv_form'.(int)$formId.$type);
	}

    /**
     * Получить список форм
     * @return array
     */
	public function getAdverticeFormList() {
		return $this->request->getRequest('GET', '/getadformlist', $this->getUri() . '&API_KEY=' . $this->APIKEY, $data=array(), 'adv_form_list' );
	}

	/**
	 * Авторизация
	 * @param string $login
	 * @param type $password
	 * @return string
	 */
	public function login($login,$password){
		return $this->request->getRequest('POST', '/login', $this->getUri(), $data=array('LOGIN'=>$login,'PASSWORD'=>$password ) );
	}

	/**
	 * Регистрация
	 * @param string $login
	 * @param type $password
	 * @param type $confirmpassword
	 * @return string
	 */
	public function register($login,$password,$confirmpassword){
		return $this->request->getRequest('POST', '/register', $this->getUri(), $data=array('LOGIN'=>$login,'PASSWORD'=>$password,'CONFIRMPASSWORD' => $confirmpassword ) );
	}


	/**
	 * Строка аутентификации
	 */
	private function getUri(){
		return 'API_KEY='.$this->APIKEY.'&LANG='.$this->lang.'&USERID='.$this->userID;
	}

	/**
	 * Обновить цену обьявления
	 * @param array $postData
	 * @return type
	 */
	public function updatePrice($postData){
		return $this->request->getRequest('POST', '/updatepricejson', $this->getUri(), $data=array('DATA'=>  json_encode($postData)) );
	}

	/**
	 * Обновить даты обьявления
	 * @param array $POST
	 * @return type
	 */
	public function updateDate($POST){
		return $this->request->getRequest('POST', '/updatedates', $this->getUri(), $data=array('DATA'=>  json_encode($POST)) );
	}

	/**
	 * Получить прайслист для обьявления
	 * @param int $advertiseId
	 */
	public function getPricelist($advertiseId){
		return $this->request->getRequest('GET', '/pricelist', $this->getUri().'&ADVERTICEID='.(int)$advertiseId, $data=array(), 'adv_price'.(int)$advertiseId);
	}

	/**
	 * Создать обьявление
	 * @param array $POST
	 */
	public function createAdvertice($POST){

		$data=array('DATA'=>  json_encode($POST));
		if(isset($_FILES)) {
			foreach ($_FILES as $key=>$file) {
				if(file_exists($file['tmp_name'])) {
					$data[$key] = new CURLFile($file['tmp_name'], $file['type'], $file['name']);
				}
			}
		}
		return $this->request->getRequest('POST', '/createad', $this->getUri(), $data );
	}

	/**
	 * Создать инвойс
	 * @param int $advId
	 */
	public function createInvoice($advId){

		return $this->request->getRequest('POST', '/createinvoice', $this->getUri(), $data=array('ADVERTICEID'=> $advId) );
	}

	/**
	 * Оплатить инвойс
	 * @param array $data
	 */
	public function payInvoice($data){
		$data['INVOICEID']=$data['invoiceId'];
		$dat=  array('DATA'=>  json_encode($data));
		return $this->request->getRequest('POST', '/payinvoice', $this->getUri()."&INVOICEID=".$data['invoiceId'], $dat );
	}

	/**
	 * Форма для оплаты
	 */
	public function getPaidForm(){

		return $this->request->getRequest('GET', '/getpayform', $this->getUri().'&TYPE=json', $data=array(),'payForm'.$this->lang );
	}

	/**
	 * Возвращает API Key
	 * @return string
	 */
	public function getApiKey(){
		return $this->APIKEY;
	}

	/**
	 * авторизует пользователя
	 * @param string $id
	 * @return string
	 */
	public function setUserId($id){
		$this->userID = $id;
		$_SESSION['userEmId'] = $this->userID;

	}

	/**
	 * Возвращает UserID
	 * @return string
	 */
	public function getUserId(){
		return $this->userID;
	}

	/**
	 * Возвращает Request
	 * @return Request
	 */
	public function getRequest(){
		return $this->request;
	}

	/**
	 * Вывести группу FB
	 * @param string $message
	 * @param mixed $data
	 */
	private function  _logGroupe($message, $data,$colapsed=true){
		$this->FB->group($message,array('Collapsed' => $colapsed));
		$this->FB->fb($data);
		$this->FB->groupEnd();
	}

	/**
	 * получить данные пользователя
	 * @return array
	 */
	public function getUserData(){
		$data=array();
		if ($this->userID) {
			$data=$this->request->getRequest('GET', '/getuserdata', $this->getUri(), $data=array(), 'userdata_'.$this->userID);
		}
		return $data;
	}

	/**
	 * Баланс
	 * @return array
	 */
	public function getBalanse(){
		$data=array();
		if ($this->userID) {
			$data=$this->request->getRequest('GET', '/getbalance', $this->getUri(), $data=array(), 'balance_'.$this->userID);
		}
		return $data;
	}

	/**
	 * Счета и платежи
	 * @param int $limit
	 * @param string $scope invoices|payments
	 * @return array
	 */
	public function getInvoices($limit=5,$scope='invoices'){
		$data=array();
		if ($this->userID) {
			$data=$this->request->getRequest(
				'GET', '/gettrans', $this->getUri().'&LIMIT='.$limit.'&SCOPE='.$scope,
				$data=array(),
				$scope.'_'.$this->userID);
		}
		return $data;
	}

	/**
	 * Данные блокнота
	 * @return array
	 */
	public function getNotepadData(){
		$data=array();
		if ($this->userID) {
			$data=$this->request->getRequest(
				'GET', '/getnotepaddata', $this->getUri(),
				$data=array(),
				'notepad_'.$this->userID);
		}
		return $data;

	}

	/**
	 * Получить личные сообщения
	 * @return array
	 */
	public function getPrivateMessage($limit=5){
		$data=array();
		if ($this->userID) {
			$data=$this->request->getRequest(
				'GET', '/getprivatemessage', $this->getUri().'&LIMIT='.$limit,
				$data=array(),
				'pm_'.$this->userID);
		}
		return $data;
	}

    /**
     * Получить объявление
     * @param int $id
     * @param string $type "html"|| "json"
     * @return array
     */
	public function getAd( $id, $type = 'json' ) {
		$data = $this->request->getRequest(
			'GET',
			'/getad',
			$this->getUri() . '&ADID=' . $id,
			$data=array(),
			'ad_'.$id
		);

		return $data;
	}

	/**
	 * Получить обьявления
	 * @param int $categoryId
	 * @param string $format html/array/idml
	 * @param int $limit
	 * @param int $page
	 * @return html/array/idml
	 */
	public function getAds($categoryId, $format,$limit=20,$page=1){
    	$data=$this->request->getRequest(
			'GET', '/getads', 'API_KEY='.$this->APIKEY.'&LANG='.$this->lang.'&CATEGORYID='.$categoryId.'&FORMAT='.$format.'&LIMIT='.$limit.'&PAGE='.$page,
			$data=array(),
			'ads_'.$categoryId.'_'.$this->lang.'_'.$format.'_'.$limit
		);

		return $data;
	}

	/**
	 * Поиск обьявлений
	 * @param array $searchParam
	 * @param string $format html/array/idml
	 * @param int $limit
	 * @param int $page
	 * @return html/array/idml
	 */
	public function searchAds($searchParam, $format,$limit=20,$page=1){
			$data=$this->request->getRequest(
				'GET', '/searchads', 'API_KEY='.$this->APIKEY.'&LANG='.$this->lang.'&SEARCHPARAM='.  json_encode($searchParam).'&FORMAT='.$format.'&LIMIT='.$limit.'&PAGE='.$page,
				$data=array()

			);
		return $data;
	}

	public function getCats() {
		$data=$this->request->getRequest(
			'GET', '/getadcategorylist', 'API_KEY=' . $this->APIKEY . '&LANG=' . $this->lang,
			$data=array(),
			'ads_cats_'.$this->lang
		);
		return $data;
	}

	/**
	 * Добавить в избранное
	 * @param string $url
	 */
	public function addToFavorites($url=''){
		$data=array();
		if ($url){
			$data=$this->request->getRequest(
				'POST', '/addtofavorites', '',
				$data=array(
					'API_KEY' => $this->APIKEY,
					'LANG'    => $this->lang,
					'URL'     => $url,
					'USERID'  => $this->userID,
				),
				null);

		}
		return $data;
	}

	/**
	 * Добавить запись в блокнот
	 * @param type $category_id
	 * @param type $text
	 * @return array
	 */
	public function addNote($category_id, $text){
		$data=array();
		if ($text){
			$data=$this->request->getRequest(
				'POST', '/addnote', '',
				$data=array(
					'API_KEY'    => $this->APIKEY,
					'LANG'       => $this->lang,
					'CATEGORYID' => $category_id,
					'TEXT'       => $text,
					'USERID'     => $this->userID,
				),
				null);

		}
		return $data;
	}

	/**
	 * Отправить личное сообщение
	 * @param type $category_id
	 * @param type $text
	 * @return array
	 */
	public function sendMail($post){
		$data=array();
		if ($post){
			$requred=array(
					'API_KEY'    => $this->APIKEY,
					'LANG'       => $this->lang,
					'USERID'     => $this->userID,
			);
			$senddata=  array_merge($requred,$post);
			$data=$this->request->getRequest(
				'POST', '/sendmail', '',
				$data=$senddata,
				null);
		}
		return $data;
	}

	/**
	 * поменять язык
	 * @param type $lang
	 */
	public function changeLang($lang){
		$haystack=array('en','ru');
		if ( in_array($lang, $haystack)) {
			SetCookie("EM_lang",$lang,time()+36000000);
			if ($this->userID){
				$this->request->flushCache($this->userID,$lang);
			}
		}
        return true;
	}
}
