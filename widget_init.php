<?php
class em_catwidget extends WP_Widget
{
public function __construct() {
        parent::__construct("em_catwidget", "Ethnic Media ads category widget",
            array("description" => "A simple widget to show categoties of ads"));
    }
public function widget( $args, $instance ) {
    $title = $instance['title'];
    $page = $instance['page'];
    $before_widget = $args['before_widget'];
    if ( ! empty( $title ) )
        $widget_title = $args['before_title'] . $title . $args['after_title'];
    $categories = EM_getAdsCats();
    $after_widget = $args['after_widget'];
    render("widget-cats",array(
        "page"         => $page,
        "title"        => $widget_title,
        "categories"   => $categories,
        "after"        => $after_widget
        ));
}

//-------------Admin-Panel------------------------------------------------------
    public function form($instance) {
        $title = "";
        $page = "";
        $pages = get_pages();
        // если instance не пустой, достанем значения
        if (!empty($instance)) {
            $title = $instance["title"];

//Указываем страницу, на которой размещен shortcode для просмотра списка объявлений
            $page = $instance["page"];
        }

        $tableId = $this->get_field_id("title");
        $tableName = $this->get_field_name("title");
?>
        <label for="<?php echo $tableId;?>">Title</label><br>
        <input id="<?php echo $tableId;?>" type="text" name="<?php echo $tableName;?>" value="<?php echo $title?>"><br>

<?php
        $pageId = $this->get_field_id("page");
        $pageName = $this->get_field_name("page");
?>

        <label for="<?php echo $pageId; ?>">Select page</label><br>
        <select id="<?php echo $pageId; ?>" name="<?php echo $pageName; ?>">
<?php foreach ($pages as $pg){
    $selected = ($pg->post_name == $page)?' selected':'';
            echo '<option value="'.$pg->post_name.'"'.$selected.'>'.$pg->post_title.'</option>';
        }
        
        echo '</select>';
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values["page"] = htmlentities($newInstance["page"]);
        return $values;
    }
}
function em_load_catwidget() {
    register_widget( 'em_catwidget' );
}
add_action( 'widgets_init', 'em_load_catwidget' );